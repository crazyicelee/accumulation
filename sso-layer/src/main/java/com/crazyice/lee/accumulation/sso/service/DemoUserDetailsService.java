package com.crazyice.lee.accumulation.sso.service;

import com.alibaba.fastjson.JSON;
import com.crazyice.lee.accumulation.sso.dao.SysUserServiceDao;
import com.crazyice.lee.accumulation.sso.pojo.SysPermission;
import com.crazyice.lee.accumulation.sso.pojo.SysUser;
import com.crazyice.lee.accumulation.sso.service.inter.PermissionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.scrypt.SCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DemoUserDetailsService implements UserDetailsService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DemoUserDetailsService.class);

    private static final PasswordEncoder passwordEncoder = new SCryptPasswordEncoder();

    @Autowired
    private SysUserServiceDao sysUserServiceDao;

    @Autowired
    private PermissionService permissionService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //根据用户名从数据库获取用户信息
        SysUser sysUser = sysUserServiceDao.getByUsername(username);
        if (null == sysUser) {
            LOGGER.warn("用户:{},不存在", username);
            throw new UsernameNotFoundException(username);
        }
        //根据用户id获取用户权限
        List<SysPermission> permissionList = permissionService.findByUserId(sysUser.getId());
        List<SimpleGrantedAuthority> authorityList = new ArrayList<>();
        permissionList.stream().forEach(item -> authorityList.add(new SimpleGrantedAuthority(item.getCode())));
        //生成用户信息进行口令验证
        User user = new User(sysUser.getUsername(), passwordEncoder.encode(sysUser.getPassword()), authorityList);
        LOGGER.info("登录用户: {}", JSON.toJSONString(user));
        return user;
    }
}
