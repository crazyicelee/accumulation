package com.crazyice.lee.accumulation.sso.dao;

import com.crazyice.lee.accumulation.sso.pojo.SysRole;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Repository;

@Mapper
@Repository
public interface SysRoleServiceDao {
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "roleName", column = "role_name"),
            @Result(property = "roleCode", column = "role_code"),
            @Result(property = "roleDescription", column = "role_description"),
            @Result(property = "createUser", column = "create_user"),
            @Result(property = "createTime", column = "create_time"),
            @Result(property = "updateUser", column = "update_user"),
            @Result(property = "updateTime", column = "update_time")
    })
    @Select("SELECT * FROM sys_role WHERE id = #{roleId}")
    SysRole getByRoleId(String roleId);
}
