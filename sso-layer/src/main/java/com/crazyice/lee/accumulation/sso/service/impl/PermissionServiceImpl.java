package com.crazyice.lee.accumulation.sso.service.impl;

import com.crazyice.lee.accumulation.sso.dao.SysPermissionServiceDao;
import com.crazyice.lee.accumulation.sso.dao.SysRolePermissionServiceDao;
import com.crazyice.lee.accumulation.sso.dao.SysUserRoleServiceDao;
import com.crazyice.lee.accumulation.sso.pojo.SysPermission;
import com.crazyice.lee.accumulation.sso.pojo.SysRolePermission;
import com.crazyice.lee.accumulation.sso.pojo.SysUserRole;
import com.crazyice.lee.accumulation.sso.service.inter.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PermissionServiceImpl implements PermissionService {

    @Autowired
    private SysUserRoleServiceDao sysUserRoleServiceDao;
    @Autowired
    private SysRolePermissionServiceDao sysRolePermissionServiceDao;
    @Autowired
    private SysPermissionServiceDao sysPermissionServiceDao;

    @Override
    public List<SysPermission> findByUserId(Integer userId) {
        List<SysUserRole> sysUserRoleList = sysUserRoleServiceDao.findByUserId(userId);
        if (CollectionUtils.isEmpty(sysUserRoleList)) {
            return null;
        }
        List<Integer> roleIdList = sysUserRoleList.stream().map(SysUserRole::getRoleId).collect(Collectors.toList());
        List<SysRolePermission> rolePermissionList = sysRolePermissionServiceDao.findByRoleIds(roleIdList);
        if (CollectionUtils.isEmpty(rolePermissionList)) {
            return null;
        }
        List<Integer> permissionIdList = rolePermissionList.stream().map(SysRolePermission::getPermissionId).distinct().collect(Collectors.toList());
        List<SysPermission> sysPermissionList = sysPermissionServiceDao.findByIds(permissionIdList);
        if (CollectionUtils.isEmpty(sysPermissionList)) {
            return null;
        }
        return sysPermissionList;
    }
}
