package com.crazyice.lee.accumulation.sso.dao;

import com.crazyice.lee.accumulation.sso.pojo.SysRolePermission;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

@Mapper
@Repository
public interface SysRolePermissionServiceDao {
    @Results({
            @Result(property = "id", column = "id"),
            @Result(property = "roleId", column = "role_id"),
            @Result(property = "permissionId", column = "permission_id")
    })
    @Select({"<script>" +
            "SELECT * FROM sys_role_permission WHERE role_id IN " +
            "<foreach collection='roleIds' item='id' open='(' separator=',' close=')'>" +
            "#{id}" +
            "</foreach>" +
            "</script>"
    })
    List<SysRolePermission> findByRoleIds(@Param("roleIds") List<Integer> roleIds);
}
