package com.crazyice.lee.accumulation.sso.service.inter;


import com.crazyice.lee.accumulation.sso.pojo.SysPermission;

import java.util.List;

/**
 * @author ChengJianSheng
 * @date 2019-02-12
 */
public interface PermissionService {

    List<SysPermission> findByUserId(Integer userId);

}
