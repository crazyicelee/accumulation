package com.crazyice.lee.accumulation.verification.web;

import com.alibaba.fastjson.JSONObject;
import com.google.code.kaptcha.Constants;
import com.google.code.kaptcha.Producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

@RestController
public class VerificationController {
    private static final Logger LOGGER = LoggerFactory.getLogger(VerificationController.class);

    @Autowired
    private Producer kaptchaProducer;

    @RequestMapping(value = "/login")
    public ModelAndView index(ModelAndView modelAndView) {
        modelAndView.setViewName("login");
        modelAndView.addObject("info", "登录");
        return modelAndView;
    }

    /**
     * 生成验证码及验证码图形
     *
     * @return
     */
    private Map<String, String> generateVerificationImage() {
        String capText = kaptchaProducer.createText();
        LOGGER.info("生成的验证码：{}", capText);
        Map<String, String> value = new HashMap<>();
        BufferedImage bi = kaptchaProducer.createImage(capText);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            ImageIO.write(bi, "jpg", outputStream);
            String base64Img = Base64.getEncoder().encodeToString(outputStream.toByteArray());
            value.put("capText", capText);
            value.put("image", base64Img);
        } catch (IOException e) {
            LOGGER.error("{}", e.getLocalizedMessage());
        }
        return value;
    }

    @RequestMapping(value = "getVerifier", method = RequestMethod.GET)
    public Map<String, String> getVerifier() {
        return generateVerificationImage();
    }

    @RequestMapping(value = "/verifier", method = RequestMethod.GET)
    public void verification(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setDateHeader("Expires", 0);
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        response.setContentType("image/jpeg");
        Map<String, String> value = generateVerificationImage();
        request.getSession().setAttribute(Constants.KAPTCHA_SESSION_CONFIG_KEY, value.get("capText"));
        ServletOutputStream out = response.getOutputStream();
        ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64.getDecoder().decode(value.get("image")));
        ImageIO.write(ImageIO.read(inputStream), "jpg", out);
        try {
            out.flush();
        } finally {
            out.close();
        }
    }

    @RequestMapping("/checkVerifier")
    public String checkVerifier(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        String captchaId = (String) httpServletRequest.getSession().getAttribute(Constants.KAPTCHA_SESSION_CONFIG_KEY);
        String parameter = httpServletRequest.getParameter("verifyCode");
        LOGGER.info("验证码：{}，输入码：{}", captchaId, parameter);
        JSONObject retString = new JSONObject();
        if (!captchaId.equalsIgnoreCase(parameter)) {
            retString.put("code", -100);
            retString.put("message", "错误的验证码");
        } else {
            retString.put("code", 0);
            retString.put("message", "验证成功");
        }
        return retString.toJSONString();
    }
}
