package com.crazyice.lee.accumulation.annotation.implement;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

@Aspect
@Component
public class FirstAspect {
    private static final Logger LOGGER = LoggerFactory.getLogger(FirstAspect.class);

    private long startRunTime = 0L;

    @Pointcut("@annotation(FirstAnnotation)")
    public void annotationPointcut() {
    }

    @Before("annotationPointcut()")
    public void beforePointcut(JoinPoint joinPoint) {
        String value = getAnnotation(joinPoint).value();
        startRunTime = System.currentTimeMillis();
        LOGGER.info("开始：{}", value);
    }

    @After("annotationPointcut()")
    public void afterPointcut(JoinPoint joinPoint) {
        String value = getAnnotation(joinPoint).value();
        LOGGER.info("结束：{}，用时：{}ms", value, System.currentTimeMillis() - startRunTime);
    }

    //从AOP获取注解对象
    private FirstAnnotation getAnnotation(JoinPoint joinPoint) {
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        Method method = methodSignature.getMethod();
        FirstAnnotation annotation = method.getAnnotation(FirstAnnotation.class);
        return annotation;
    }
}
