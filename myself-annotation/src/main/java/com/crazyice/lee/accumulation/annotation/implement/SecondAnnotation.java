package com.crazyice.lee.accumulation.annotation.implement;

import com.crazyice.lee.accumulation.annotation.data.Sex;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * Bean注册的方式注解
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({SecondBeanDefinitionRegistrar.class})
public @interface SecondAnnotation {
    String name() default "姓名";

    int age() default 18;

    Sex sex() default Sex.MALE;
}
