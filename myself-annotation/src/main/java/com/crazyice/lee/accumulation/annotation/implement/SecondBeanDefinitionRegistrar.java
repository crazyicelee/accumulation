package com.crazyice.lee.accumulation.annotation.implement;

import com.crazyice.lee.accumulation.annotation.data.Person;
import com.crazyice.lee.accumulation.annotation.data.Sex;
import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.support.BeanDefinitionReaderUtils;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.GenericBeanDefinition;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.annotation.AnnotationAttributes;
import org.springframework.core.type.AnnotationMetadata;

public class SecondBeanDefinitionRegistrar implements ImportBeanDefinitionRegistrar {
    @Override
    public void registerBeanDefinitions(AnnotationMetadata annotationMetadata, BeanDefinitionRegistry beanDefinitionRegistry) {
        AnnotationAttributes attributes = AnnotationAttributes.fromMap(annotationMetadata.getAnnotationAttributes(SecondAnnotation.class.getName()));
        String name = attributes.getString("name");
        int age = attributes.getNumber("age");
        Sex sex = attributes.getEnum("sex");
        GenericBeanDefinition beanDefinition = new GenericBeanDefinition();
        beanDefinition.setBeanClass(Person.class);
        beanDefinition.getPropertyValues().add("name", name);
        beanDefinition.getPropertyValues().add("age", age);
        beanDefinition.getPropertyValues().add("sex", sex);
        BeanDefinitionHolder holder = new BeanDefinitionHolder(beanDefinition, "person");
        BeanDefinitionReaderUtils.registerBeanDefinition(holder, beanDefinitionRegistry);
    }
}
