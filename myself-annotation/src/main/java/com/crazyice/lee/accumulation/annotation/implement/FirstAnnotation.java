package com.crazyice.lee.accumulation.annotation.implement;

import java.lang.annotation.*;

/**
 * AOP方式注解
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FirstAnnotation {
    String value() default "AOP类型的注解";
}
