package com.crazyice.lee.accumulation.annotation.data;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Data
public class Person {
    private static final Logger LOGGER = LoggerFactory.getLogger(Person.class);
    private String name;
    private int age;
    private Sex sex;

    public String sayHello() {
        LOGGER.info("你好：{}", name);
        return name;
    }
}
