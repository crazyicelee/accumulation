package com.crazyice.lee.accumulation.annotation.data;

public enum Sex {
    MALE("男"), FAMALE("女");

    private final String sex;

    Sex(String sex) {
        this.sex = sex;
    }

    @Override
    public String toString() {
        return this.sex;
    }
}
