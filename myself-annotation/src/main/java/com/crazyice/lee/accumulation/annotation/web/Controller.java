package com.crazyice.lee.accumulation.annotation.web;

import com.crazyice.lee.accumulation.annotation.data.Person;
import com.crazyice.lee.accumulation.annotation.data.Sex;
import com.crazyice.lee.accumulation.annotation.implement.FirstAnnotation;
import com.crazyice.lee.accumulation.annotation.implement.SecondAnnotation;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
//通过注解实例化Bean person(自定义注解)
@SecondAnnotation(name = "蔡云飞", age = 30, sex = Sex.FAMALE)
public class Controller {
    private static final Logger LOGGER = LoggerFactory.getLogger(Controller.class);

    @Autowired
    private Person person;

    //自定义注解（AOP方式）
    @FirstAnnotation("第一个注解")
    @RequestMapping(value = "/testAnnotation", method = RequestMethod.GET)
    @ApiOperation(value = "注解测试", notes = "AOP方式注解验证")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "message", value = "动态信息", required = true, dataType = "String")
    })
    public String login(@RequestParam(value = "message") String message) {
        LOGGER.info("{}:{}-{}", message, person, person.sayHello());
        return message;
    }
}
