package com.crazyice.lee.accumulation.file;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.OSSObject;
import com.aliyun.oss.model.OSSObjectSummary;
import com.aliyun.oss.model.ObjectListing;
import com.crazyice.lee.accumulation.file.conf.OssConf;
import com.crazyice.lee.accumulation.file.service.HdfsService;
import lombok.extern.slf4j.Slf4j;
import org.apache.hadoop.fs.BlockLocation;
import org.apache.hadoop.fs.FSDataInputStream;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.*;
import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ApplicationTests {

    @Autowired
    private HdfsService hdfsService;

    @Autowired
    private OssConf ossConf;

    //@Test
    public void testOssCreateBucket() {
        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(ossConf.getEndpoint(), ossConf.getAccessKeyId(), ossConf.getAccessKeySecret());

        // 创建存储空间。
        ossClient.createBucket(ossConf.getBucketName());

        // 关闭OSSClient。
        ossClient.shutdown();
    }

    //@Test
    public void testOssUpload() throws Exception {
        String objectName = "test";
        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(ossConf.getEndpoint(), ossConf.getAccessKeyId(), ossConf.getAccessKeySecret());

        // 上传内容到指定的存储空间（bucketName）并保存为指定的文件名称（objectName）。
        String contentWrite = "Hello OSS";
        ossClient.putObject(ossConf.getBucketName(), objectName, new ByteArrayInputStream(contentWrite.getBytes()));

        // 调用ossClient.getObject返回一个OSSObject实例，该实例包含文件内容及文件元信息。
        OSSObject ossObject = ossClient.getObject(ossConf.getBucketName(), objectName);
        // 调用ossObject.getObjectContent获取文件输入流，可读取此输入流获取其内容。
        InputStream contentRead = ossObject.getObjectContent();
        if (contentRead != null) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(contentRead));
            while (true) {
                String line = reader.readLine();
                if (line == null) break;
                log.info("{}", line);
            }
            // 数据读取完成后，获取的流必须关闭，否则会造成连接泄漏，导致请求无连接可用，程序无法正常工作。
            contentRead.close();
        }

        // 关闭OSSClient。
        ossClient.shutdown();
    }

    //@Test
    public void testOssUploadFile() {
        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(ossConf.getEndpoint(), ossConf.getAccessKeyId(), ossConf.getAccessKeySecret());

        // 上传本地文件
        ossClient.putObject(ossConf.getBucketName(), "test1", new File("/Users/crazyicelee/mywokerspace/accumulation/document/pic01.png"));

        // 关闭OSSClient。
        ossClient.shutdown();
    }

    @Test
    public void testOssList() {
        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(ossConf.getEndpoint(), ossConf.getAccessKeyId(), ossConf.getAccessKeySecret());

        // 获取资源列表
        ObjectListing objectListing = ossClient.listObjects(ossConf.getBucketName());
        for (OSSObjectSummary ossObjectSummary : objectListing.getObjectSummaries()) {
            log.info("云存储对象：{}-{}", ossObjectSummary.getKey(), ossObjectSummary.getSize());
        }
        // 关闭OSSClient。
        ossClient.shutdown();
    }

    /**
     * 测试创建HDFS目录
     */
    //@Test
    public void testMkdir() {
        boolean result1 = hdfsService.mkdir("/testDir0");
        log.info("创建结果：{}", result1);
    }

    /**
     * 测试上传文件
     */
    //@Test
    public void testUploadFile() {
        String path = "/Users/crazyicelee/Downloads/flink-1.8.1";
        hdfsService.uploadFileToHdfs(path, "/testDir");
    }

    /**
     * 测试列出某个目录下面的文件
     */
    //@Test
    public void testListFiles() {
        List<Map<String, Object>> result = hdfsService.listFiles("/testDir", null);

        result.forEach(fileMap -> {
            fileMap.forEach((key, value) -> {
                if (key.equalsIgnoreCase("path")) {
                    log.info("{}", value);
                }
            });
        });
    }

    /**
     * 测试下载文件
     */
    //@Test
    public void testDownloadFile() {
        hdfsService.downloadFileFromHdfs("/testDir/wordcount.txt", "/Users/crazyicelee/test111.txt");
    }

    /**
     * 测试打开HDFS上面的文件
     */
    //@Test
    public void testOpen() throws IOException {
        FSDataInputStream inputStream = hdfsService.open("/testDir/wordcount.txt");

        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String line = null;
        while ((line = reader.readLine()) != null) {
            log.info("{}", line);
        }

        reader.close();
    }

    /**
     * 测试打开HDFS上面的文件，并转化为Java对象
     */
    //@Test
    public void testOpenWithObject() throws IOException {
        //SysUserEntity user = hdfsService.openWithObject("/testDir/b.txt", SysUserEntity.class);
        //log.info(user);
    }

    /**
     * 测试重命名
     */
    //@Test
    public void testRename() {
        hdfsService.rename("/testDir/wordcount.txt", "/testDir/wordcount.bak.txt");

        //再次遍历
        testListFiles();
    }

    /**
     * 测试删除文件
     */
    //@Test
    public void testDelete() {
        hdfsService.delete("/testDir/wordcount.bak.txt");

        //再次遍历
        testListFiles();
    }

    /**
     * 测试获取某个文件在HDFS集群的位置
     */
    //@Test
    public void testGetFileBlockLocations() throws IOException {
        BlockLocation[] locations = hdfsService.getFileBlockLocations("/testDir/nohup.out");

        if (locations != null && locations.length > 0) {
            for (BlockLocation location : locations) {
                log.info(location.getHosts()[0]);
            }
        }
    }
}
