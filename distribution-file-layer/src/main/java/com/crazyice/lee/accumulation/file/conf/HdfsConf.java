package com.crazyice.lee.accumulation.file.conf;

import com.crazyice.lee.accumulation.file.service.HdfsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Properties;

@Configuration
@Slf4j
public class HdfsConf {

    @Value("${hdfsUrl}")
    private String defaultHdfsUri;

    @Bean
    public HdfsService getHdfsService() {
        Properties properties = System.getProperties();
        properties.setProperty("HADOOP_USER_NAME", "root");
        org.apache.hadoop.conf.Configuration conf = new org.apache.hadoop.conf.Configuration();
        conf.set("fs.defaultFS", defaultHdfsUri);
        conf.set("fs.hdfs.impl", "org.apache.hadoop.hdfs.DistributedFileSystem");
        conf.set("dfs.client.block.write.replace-datanode-on-failure.policy", "NEVER");
        return new HdfsService(conf, defaultHdfsUri);
    }
}
