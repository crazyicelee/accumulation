package com.crazyice.lee.accumulation.file.pojo;

import lombok.Data;

@Data
public class Face {
    String app_id;
    String app_secret;
    String groupId;
    String userId;
    String name;
    String img;
}
