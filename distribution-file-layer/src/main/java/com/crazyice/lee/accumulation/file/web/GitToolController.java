package com.crazyice.lee.accumulation.file.web;

import cn.hutool.core.date.DateUtil;
import cn.hutool.http.Header;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.crazyice.lee.accumulation.file.utils.XmlUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@RestController
@Slf4j
@RequestMapping("/git")
public class GitToolController {

    private String httpRequest(String URL) {
        String result = "";
        try {
            HttpResponse hr = HttpRequest.get(URL)
                    .header(Header.HOST, "toongitlab.syswin.com")
                    .cookie("_gitlab_session=0cdc51f0d296f414d60a45d526dfb6c5")
                    .timeout(50000)//超时，毫秒
                    .execute();
            if (hr.getStatus() == 200) {
                result = hr.body();
            }
        } catch (Exception e) {
            log.error("{}调用异常\n{}", URL, e.getLocalizedMessage());
        }
        return result;
    }

    @RequestMapping(value = "/pom", method = RequestMethod.GET)
    public Map getPom(@RequestParam("num") Integer num) {
        Map<String, Integer> projectBranchesNum = new HashMap<>();
        List<Integer> ids = new ArrayList<>();
        for (int i = 0; i < num; i++) {
            ids.add(i + 1);
        }
        List<String> pomList = new ArrayList<>();
        //获取项目信息，提取最新分支
        ids.parallelStream().forEach(id -> {
            try {
                String projectInfo = httpRequest("http://toongitlab.syswin.com/api/v4/projects/" + id);
                if (!projectInfo.isEmpty()) {
                    JSON project = JSONUtil.parse(projectInfo);
                    String webUrl = (String) project.getByPath("web_url");
                    String branchesInfo = httpRequest("http://toongitlab.syswin.com/api/v4/projects/" + id + "/repository/branches");
                    if (!branchesInfo.isEmpty()) {
                        JSONArray branches = JSONUtil.parseArray(branchesInfo);
                        projectBranchesNum.put((String) project.getByPath("name_with_namespace"), branches.size());
                        Object[] tmp = branches.stream().sorted((o1, o2) -> {
                            JSON branch1 = JSONUtil.parse(o1);
                            JSON commit1 = branch1.getByPath("commit", JSON.class);
                            Date committedDate1 = DateUtil.parse((String) commit1.getByPath("committed_date"));
                            JSON branch2 = JSONUtil.parse(o2);
                            JSON commit2 = branch2.getByPath("commit", JSON.class);
                            Date committedDate2 = DateUtil.parse((String) commit2.getByPath("committed_date"));
                            return committedDate2.compareTo(committedDate1);
                        }).toArray();
                        if (tmp.length > 0) {
                            pomList.add(webUrl + "/raw/" + JSONUtil.parse(tmp[0]).getByPath("name") + "/pom.xml");
                        }
                    }
                }
            } catch (Exception e) {
                log.error("{}", id);
            }
        });
        log.info("完成分支数量统计");

        //扫描pom文件，过滤出java项目及java项目包含的模块
        AtomicInteger javaNum= new AtomicInteger();
        List<String> javaProjects = new ArrayList<>();
        pomList.parallelStream().forEach(pom -> {
            String result = httpRequest(pom);
            if (!result.isEmpty()) {
                javaNum.getAndIncrement();
                javaProjects.add(pom);
                try {
                    JSONObject document = XmlUtils.xml2Json(result);
                    JSONObject project = document.getJSONObject("project");
                    //获取子模块信息
                    JSONArray modules = project.getJSONArray("modules");
                    if(modules!=null) {
                        modules.parallelStream().forEach(o -> {
                            String modulePom = pom.replace("pom.xml", new JSONObject(o).get("module") + "/pom.xml");
                            javaProjects.add(modulePom);
                        });
                    }
                } catch (Exception e) {
                    log.error("modules:{}", pom);
                }
            }
        });
        log.info("完成java项目POM文件提取");

        //解析pom.xml文件读取工程信息
        Map<String,Object> projectParent=new HashMap<>();

        Map<String, Integer> thirdJars = new HashMap<>();
        Map<String, Map<String, Integer>> table = new HashMap<>();
        javaProjects.parallelStream().forEach(pom -> {
            String result = httpRequest(pom);
            if (!result.isEmpty()) {
                try {
                    JSONObject document = XmlUtils.xml2Json(result);
                    JSONObject project = document.getJSONObject("project");
                    //获取项目数据
                    String projectName = project.get("groupId")
                            + "."
                            + project.get("artifactId");
                    //获取parent信息
                    JSONArray parent=project.getJSONArray("parent");
                    if(parent!=null){
                        projectParent.put(projectName,parent.get(0));
                    }
                    //获取依赖包信息
                    Map<String, Integer> cell = new HashMap<>();
                    if(project.getJSONArray("dependencies")!=null) {
                        JSONArray dependencies = project.getJSONArray("dependencies").getJSONObject(0).getJSONArray("dependency");
                        dependencies.parallelStream().forEach(o -> {
                            JSONObject dependency = new JSONObject(o);
                            String packageName = dependency.get("groupId")
                                    + "."
                                    + dependency.get("artifactId");
                            cell.put(packageName, 1);
                        });
                        table.put(projectName, cell);
                        cell.forEach((key, value) -> thirdJars.merge(key, value, (v1, v2) -> v1 +  v2));
                    }
                } catch (Exception e) {
                    log.error("dependencies:{}", pom);
                }
            }
        });

        //依赖包调用次数排序（降序排序）
        List<Map.Entry<String,Integer>> listThirdJars = new ArrayList<Map.Entry<String,Integer>>(thirdJars.entrySet());
        Collections.sort(listThirdJars, (o1, o2) -> o2.getValue().compareTo(o1.getValue()));

        //处理table
        /*
        title.forEach((key,value)->title.put(key,0));
        ArrayList<Map<String,Object>> rows=new ArrayList<>();
        table.forEach((key,value)->{
            Map<String,Object> tmp1=new HashMap<>();
            tmp1.put("项目",key);
            tmp1.putAll(title);
            tmp1.forEach((key1,value1)->value.merge(key1,value1,(v1,v2)->(Integer)v1+(Integer) v2));
            rows.add(value);
        });

        ExcelWriter writer = ExcelUtil.getWriter("lee.xlsx");
        writer.write(rows, true);
        writer.close();
        */
        log.info("完成分析{}个git项目中的{}个java项目的{}模块", num, javaNum, javaProjects.size());
        Map<String, Object> returnResult = new HashMap<>();
        returnResult.put("parent",projectParent);
        returnResult.put("projectBranchesNum", projectBranchesNum);
        returnResult.put("dependency", listThirdJars);
        return returnResult;
    }
}
