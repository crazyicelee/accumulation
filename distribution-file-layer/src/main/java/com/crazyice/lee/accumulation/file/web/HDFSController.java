package com.crazyice.lee.accumulation.file.web;

import com.crazyice.lee.accumulation.file.service.HdfsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/file/hdfs")
@Slf4j
public class HDFSController {

    @Autowired
    private HdfsService hdfsService;

    @PostMapping(value = "/mkdir")
    public Boolean save(@RequestParam(value = "path") String path) {
        boolean result = hdfsService.mkdir(path);
        return result;
    }

}
