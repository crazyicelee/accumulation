package com.crazyice.lee.accumulation.file.web;

import com.alibaba.fastjson.JSONObject;
import com.baidu.aip.face.AipFace;
import com.crazyice.lee.accumulation.file.conf.FaceID;
import com.crazyice.lee.accumulation.file.pojo.Face;
import com.crazyice.lee.accumulation.file.utils.ImageTools;
import com.systoon.scloud.master.lightsdk.request.BaseRequestDownload;
import com.systoon.scloud.master.lightsdk.server.BaseDownloadService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@Slf4j
@RequestMapping("/f")
public class SyswinDFSController {
    @Autowired
    private FaceID faceID;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String getPrivateUrl(@PathVariable("id") String id) {
        log.info("{}", faceID);
        String result = "";
        try {
            BaseRequestDownload baseRequestDownload = new BaseRequestDownload();
            // 1. 私有云下载
            baseRequestDownload.setStoid(id);
            baseRequestDownload.setClientIp("172.1.2.1");
            baseRequestDownload.setLocation("111.132,123.123");
            baseRequestDownload.setReturnMimeType("image/png");
            baseRequestDownload.setExpires(36000);
            BaseDownloadService downloadService = new BaseDownloadService(2001, "scloud.beijingtoon.com", "oq6wpq5661m75rpl63w2qv08", "beijingtoon");
            // 2. 获取服务器资源路径
            String url = downloadService.directDownloadUrl(baseRequestDownload);
            // 3. 图片转化为base64
            String base64Img = ImageTools.imageToBase64ByOnline(new URL(url));
            // 4. 上传到人脸库
            Face face = new Face();
            face.setApp_id("system");
            face.setApp_secret("12345");
            face.setGroupId("1000001");
            face.setName(id.substring(0, 10));
            face.setUserId(id.substring(10, 20));
            face.setImg(base64Img);
            result = ImageTools.doPost("http://103.83.45.187:7100/face/add", JSONObject.toJSONString(face));
        } catch (Exception ex) {
            log.error("下载文件异常", ex);
        }
        return result;
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public String uploadFaces(@RequestParam("path") String path) throws IOException {
        String result = "";
        final Pattern pattern = Pattern.compile("(?i).+?\\.(jpg|gif|png)");
        Files.walkFileTree(Paths.get(path), new SimpleFileVisitor<Path>() {
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Matcher m = pattern.matcher(file.toString());
                if (m.matches()) {
                    //1.读取本地文件
                    File f = file.toFile();
                    if (f.getName().endsWith("_1.png")) {
                        String name = f.getAbsolutePath();
                        String base64Img = ImageTools.imageToBase64ByLocal(name);
                        //2.上传到人脸库
                        Face face = new Face();
                        face.setApp_id("system");
                        face.setApp_secret("12345");
                        face.setGroupId("1000001");
                        face.setName(f.getName());
                        face.setUserId(f.getName());
                        face.setImg(base64Img);
                        String rs = ImageTools.doPost("http://103.83.45.187:7100/face/add", JSONObject.toJSONString(face));
                        log.info("{}", rs);
                    }
                }
                return FileVisitResult.CONTINUE;
            }
        });
        return result;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addFaces(@RequestParam("path") String path) throws IOException {
        //设置APPID/AK/SK
        String APP_ID = "16433812";
        String API_KEY = "dakWpyji90qAcTgi56QLYsek";
        String SECRET_KEY = "y4beb2YqcBqYq4SLemXyxz4N7QjZbMIC";
        // 初始化一个AipFace
        AipFace client = new AipFace(APP_ID, API_KEY, SECRET_KEY);

        String result = "";
        final Pattern pattern = Pattern.compile("(?i).+?\\.(jpg|gif|png)");
        Files.walkFileTree(Paths.get(path), new SimpleFileVisitor<Path>() {
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Matcher m = pattern.matcher(file.toString());
                if (m.matches()) {
                    //1.读取本地文件
                    File f = file.toFile();
                    if (f.getName().endsWith("_2.png")) {
                        String name = f.getAbsolutePath();
                        String base64Img = ImageTools.imageToBase64ByLocal(name);
                        //2.上传到人脸库
                        // 传入可选参数调用接口
                        HashMap<String, String> options = new HashMap<String, String>();
                        options.put("user_info", "user's info");
                        options.put("quality_control", "NORMAL");
                        options.put("liveness_control", "LOW");
                        options.put("action_type", "REPLACE");
                        String imageType = "BASE64";
                        String groupId = "100000";
                        String userId = f.getName().replaceAll("\\.", "_");
                        // 人脸注册
                        org.json.JSONObject res = client.addUser(base64Img, imageType, groupId, userId, options);
                        log.info("{}", res);
                        try {
                            Thread.sleep(300);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                return FileVisitResult.CONTINUE;
            }
        });
        return result;
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public String searchFaces(@RequestParam("img") String img) throws IOException {
        //设置APPID/AK/SK
        String APP_ID = "16433812";
        String API_KEY = "dakWpyji90qAcTgi56QLYsek";
        String SECRET_KEY = "y4beb2YqcBqYq4SLemXyxz4N7QjZbMIC";
        // 初始化一个AipFace
        AipFace client = new AipFace(APP_ID, API_KEY, SECRET_KEY);
        // 传入可选参数调用接口
        HashMap<String, String> options = new HashMap<String, String>();
        options.put("user_info", "user's info");
        options.put("quality_control", "NORMAL");
        options.put("liveness_control", "LOW");
        options.put("action_type", "REPLACE");
        String result = client.search(img, "BASE64", "100000", options).toString();
        return result;
    }
}
