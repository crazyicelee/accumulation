package com.crazyice.lee.accumulation.file.web;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.client.gridfs.GridFSBucket;
import com.mongodb.client.gridfs.GridFSDownloadStream;
import com.mongodb.client.gridfs.model.GridFSFile;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsResource;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.UUID;

@RestController
@RequestMapping("/file")
public class GridFSController {

    private static Logger LOGGER = LoggerFactory.getLogger(GridFSController.class);
    @Autowired
    private GridFsTemplate gridFsTemplate;
    @Autowired
    private GridFSBucket gridFSBucket;

    @PostMapping(value = "/upload")
    public String save(@RequestPart(value = "file") MultipartFile file) {
        LOGGER.info("Saving file..");
        DBObject metaData = new BasicDBObject();
        metaData.put("createdDate", new Date());

        String fileName = UUID.randomUUID().toString();
        try {
            InputStream inputStream = file.getInputStream();
            gridFsTemplate.store(inputStream, fileName, file.getContentType(), metaData);
            LOGGER.info("文件存储：{}", fileName);
            //生成图片文件的缩略图
            switch (file.getContentType()) {
                case MediaType.IMAGE_JPEG_VALUE:
                case MediaType.IMAGE_GIF_VALUE:
                case MediaType.IMAGE_PNG_VALUE:
                    int width = 80;
                    int height = 80;
                    String scaledFilename = fileName + "-" + width + "x" + height;

                    BufferedImage bufferedImage = ImageIO.read(file.getInputStream());
                    Image image80x80 = bufferedImage.getScaledInstance(width, height, Image.SCALE_SMOOTH);
                    BufferedImage _bufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
                    Graphics graphics = _bufferedImage.getGraphics();
                    graphics.drawImage(image80x80, 0, 0, null);
                    graphics.dispose();
                    ByteArrayOutputStream _bbos = new ByteArrayOutputStream();
                    ImageIO.write(_bufferedImage, "JPG", _bbos);
                    _bbos.flush();
                    _bbos.close();
                    inputStream = new ByteArrayInputStream(_bbos.toByteArray());

                    gridFsTemplate.store(inputStream, scaledFilename, file.getContentType(), metaData);
                    LOGGER.info("缩略图存储：{}", scaledFilename);
            }
            inputStream.close();
        } catch (IOException e) {
            LOGGER.error("IOException: " + e);
        }
        return fileName;
    }

    @RequestMapping(value = "/download/{fileName}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] get(@PathVariable("fileName") String fileName) throws IOException {
        LOGGER.info("Getting file.." + fileName);
        GridFSFile result = gridFsTemplate.findOne(new Query().addCriteria(Criteria.where("filename").is(fileName)));
        if (result == null) {
            LOGGER.info("File not found" + fileName);
            throw new RuntimeException("No file with name: " + fileName);
        }
        LOGGER.info("File found " + fileName);
        //打开流下载对象
        GridFSDownloadStream downloadStream = gridFSBucket.openDownloadStream(result.getObjectId());
        //获取流对象
        GridFsResource gridFsResource = new GridFsResource(result, downloadStream);
        return IOUtils.toByteArray(gridFsResource.getInputStream());
    }

    @RequestMapping(value = "/delete/{fileName}", method = RequestMethod.DELETE)
    public void delete(@PathVariable(value = "fileName") String fileName) {
        LOGGER.info("Deleting file.." + fileName);
        gridFsTemplate.delete(new Query().addCriteria(Criteria.where("filename").is(fileName)));
        LOGGER.info("File deleted " + fileName);
    }
}
