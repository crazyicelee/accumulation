package com.crazyice.lee.accumulation.file.service;

import com.alibaba.fastjson.JSON;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@AllArgsConstructor
public class HdfsService {

    private Configuration conf;
    private String defaultHdfsUri;

    private FileSystem getFileSystem() throws IOException {
        return FileSystem.get(conf);
    }

    public boolean mkdir(String path) {
        //如果目录已经存在，则直接返回
        if (checkExists(path)) {
            return true;
        } else {
            try (FileSystem fileSystem = getFileSystem()) {
                //最终的HDFS文件目录
                String hdfsPath = generateHdfsPath(path);
                //创建目录
                return fileSystem.mkdirs(new Path(hdfsPath));
            } catch (IOException e) {
                log.error("创建HDFS目录失败，path:{},{}", path, e.getLocalizedMessage());
                return false;
            }
        }
    }

    public void uploadFileToHdfs(String srcFile, String dstPath) {
        File file = new File(srcFile);
        if (file.exists()) {
            if (file.isDirectory()) {
                File[] fs = file.listFiles();
                for (File f : fs) {
                    if (f.isDirectory())
                        uploadFileToHdfs(f.getPath(), dstPath);
                    if (f.isFile()) {
                        this.uploadFileToHdfs(false, true, f.getPath(), dstPath);
                    }
                }
            } else {
                this.uploadFileToHdfs(false, true, srcFile, dstPath);
            }
        }
    }

    public void uploadFileToHdfs(boolean delSrc, boolean overwrite, String srcFile, String dstPath) {
        //源文件路径
        Path localSrcPath = new Path(srcFile);
        //目标文件路径
        Path hdfsDstPath = new Path(generateHdfsPath(dstPath));

        try (FileSystem fileSystem = getFileSystem()) {
            log.info("开始上传文件{} ... ...", srcFile);
            fileSystem.copyFromLocalFile(delSrc, overwrite, localSrcPath, hdfsDstPath);
            log.info("上传文件到HDFS完成。");
        } catch (IOException e) {
            log.error("上传文件至HDFS失败，srcFile:{},dstPath:{}", srcFile, dstPath);
        }
    }

    public boolean checkExists(String path) {
        try (FileSystem fileSystem = getFileSystem()) {
            //最终的HDFS文件目录
            String hdfsPath = generateHdfsPath(path);

            //创建目录
            return fileSystem.exists(new Path(hdfsPath));
        } catch (IOException e) {
            log.error("判断文件或者目录是否在HDFS上面存在失败，path:{}", path);
            return false;
        }
    }

    public List<Map<String, Object>> listFiles(String path, PathFilter pathFilter) {
        //返回数据
        List<Map<String, Object>> result = new ArrayList<>();

        //如果目录已经存在，则继续操作
        if (checkExists(path)) {
            try (FileSystem fileSystem = getFileSystem()) {
                //最终的HDFS文件目录
                String hdfsPath = generateHdfsPath(path);

                FileStatus[] statuses;
                //根据Path过滤器查询
                if (pathFilter != null) {
                    statuses = fileSystem.listStatus(new Path(hdfsPath), pathFilter);
                } else {
                    statuses = fileSystem.listStatus(new Path(hdfsPath));
                }

                if (statuses != null) {
                    for (FileStatus status : statuses) {
                        //每个文件的属性
                        Map<String, Object> fileMap = new HashMap<>(2);

                        fileMap.put("path", status.getPath().toString());
                        fileMap.put("isDir", status.isDirectory());

                        result.add(fileMap);
                    }
                }
            } catch (IOException e) {
                log.error("获取HDFS上面的某个路径下面的所有文件失败，path:{}", path);
            }
        }

        return result;
    }

    public void downloadFileFromHdfs(String srcFile, String dstFile) {
        //HDFS文件路径
        Path hdfsSrcPath = new Path(generateHdfsPath(srcFile));
        //下载之后本地文件路径
        Path localDstPath = new Path(dstFile);

        try (FileSystem fileSystem = getFileSystem()) {
            fileSystem.copyToLocalFile(hdfsSrcPath, localDstPath);
        } catch (IOException e) {
            log.error("从HDFS下载文件至本地失败，srcFile:{},dstFile:{}", srcFile, dstFile);
        }
    }

    public FSDataInputStream open(String path) throws IOException {
        //HDFS文件路径
        Path hdfsPath = new Path(generateHdfsPath(path));

        FileSystem fileSystem = getFileSystem();
        return fileSystem.open(hdfsPath);
    }

    public byte[] openWithBytes(String path) {
        //HDFS文件路径
        Path hdfsPath = new Path(generateHdfsPath(path));

        try (FileSystem fileSystem = getFileSystem(); FSDataInputStream inputStream = fileSystem.open(hdfsPath)) {
            return IOUtils.toByteArray(inputStream);
        } catch (IOException e) {
            log.error("打开HDFS上面的文件失败，path:{}", path);
        }

        return null;
    }

    public String openWithString(String path) {
        //HDFS文件路径
        Path hdfsPath = new Path(generateHdfsPath(path));

        try (FileSystem fileSystem = getFileSystem(); FSDataInputStream inputStream = fileSystem.open(hdfsPath)) {
            return IOUtils.toString(inputStream, Charset.forName("UTF-8"));
        } catch (IOException e) {
            log.error("打开HDFS上面的文件失败，path:{}", path);
        }

        return null;
    }

    public <T extends Object> T openWithObject(String path, Class<T> clazz) {
        //1、获得文件的json字符串
        String jsonStr = this.openWithString(path);

        //2、使用com.alibaba.fastjson.JSON将json字符串转化为Java对象并返回
        return JSON.parseObject(jsonStr, clazz);
    }

    public boolean rename(String srcFile, String dstFile) {
        //HDFS文件路径
        Path srcFilePath = new Path(generateHdfsPath(srcFile));
        //下载之后本地文件路径
        Path dstFilePath = new Path(dstFile);

        try (FileSystem fileSystem = getFileSystem()) {
            return fileSystem.rename(srcFilePath, dstFilePath);
        } catch (IOException e) {
            log.error("重命名失败，srcFile:{},dstFile:{}", srcFile, dstFile);
        }

        return false;
    }

    public boolean delete(String path) {
        //HDFS文件路径
        Path hdfsPath = new Path(generateHdfsPath(path));

        try (FileSystem fileSystem = getFileSystem()) {
            return fileSystem.delete(hdfsPath, true);
        } catch (IOException e) {
            log.error("删除HDFS文件或目录失败，path:{}", path);
        }

        return false;
    }

    public BlockLocation[] getFileBlockLocations(String path) {
        //HDFS文件路径
        Path hdfsPath = new Path(generateHdfsPath(path));

        try (FileSystem fileSystem = getFileSystem()) {
            FileStatus fileStatus = fileSystem.getFileStatus(hdfsPath);
            return fileSystem.getFileBlockLocations(fileStatus, 0, fileStatus.getLen());
        } catch (IOException e) {
            log.error("获取某个文件在HDFS集群的位置失败，path:{}", path);
        }

        return null;
    }

    private String generateHdfsPath(String dstPath) {
        String hdfsPath = defaultHdfsUri;
        if (dstPath.startsWith("/")) {
            hdfsPath += dstPath;
        } else {
            hdfsPath = hdfsPath + "/" + dstPath;
        }

        return hdfsPath;
    }
}
