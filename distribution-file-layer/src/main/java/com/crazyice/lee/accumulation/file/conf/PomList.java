package com.crazyice.lee.accumulation.file.conf;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Data
@Configuration
@ConfigurationProperties(prefix = "pomlist")
public class PomList {
    private List<String> poms;
}
