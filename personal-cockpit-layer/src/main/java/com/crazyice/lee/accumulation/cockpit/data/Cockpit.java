package com.crazyice.lee.accumulation.cockpit.data;

import lombok.Data;

import java.util.List;

@Data
public class Cockpit {
    private String userId;
    private List<String> models;
}
