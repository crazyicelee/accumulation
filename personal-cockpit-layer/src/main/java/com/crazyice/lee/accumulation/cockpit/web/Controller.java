package com.crazyice.lee.accumulation.cockpit.web;

import com.alibaba.fastjson.JSONObject;
import com.crazyice.lee.accumulation.cockpit.conf.DictionaryConfig;
import com.crazyice.lee.accumulation.cockpit.data.Cockpit;
import com.crazyice.lee.accumulation.cockpit.data.Model;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class Controller {
    private static Logger log = LoggerFactory.getLogger(Controller.class);

    @Autowired
    private DictionaryConfig dictionaryConfig;

    @RequestMapping(value = "/getCockpit/{userId}", method = RequestMethod.GET)
    @ApiOperation(value = "个人驾驶舱", notes = "获取指定用户的个性化驾驶舱数据")
    public JSONObject getDictionary(@PathVariable("userId") String userId) {
        List<Cockpit> cockpits = dictionaryConfig.getCockpits();
        List<Model> models = dictionaryConfig.getModels();

        Cockpit cockpit = cockpits.parallelStream().filter(object ->
                userId.equalsIgnoreCase(object.getUserId())
        ).findFirst().get();

        if (cockpit != null) {
            List<Model> subModels = models.parallelStream().filter(object ->
                    cockpit.getModels().contains(object.getId())
            ).collect(Collectors.toList());

            JSONObject returnValue = new JSONObject();
            returnValue.put("userId", userId);
            returnValue.put("models", subModels);

            return returnValue;
        }
        return null;
    }
}
