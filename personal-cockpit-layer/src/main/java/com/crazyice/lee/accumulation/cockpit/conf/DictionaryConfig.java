package com.crazyice.lee.accumulation.cockpit.conf;

import com.crazyice.lee.accumulation.cockpit.data.Cockpit;
import com.crazyice.lee.accumulation.cockpit.data.Model;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * 数据字典bean，动态从consul获取
 **/
@Configuration
@ConfigurationProperties(prefix = "personal")
@Data
public class DictionaryConfig {
    private List<Cockpit> cockpits;
    private List<Model> models;
}
