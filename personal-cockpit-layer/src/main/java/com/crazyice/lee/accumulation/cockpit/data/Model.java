package com.crazyice.lee.accumulation.cockpit.data;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.util.List;

@Data
public class Model {
    private String id;
    private String type;
    private String name;
    private List<JSONObject> data;
}
