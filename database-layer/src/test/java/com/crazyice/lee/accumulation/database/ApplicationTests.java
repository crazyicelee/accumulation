package com.crazyice.lee.accumulation.database;

import com.crazyice.lee.accumulation.database.data.Email;
import com.crazyice.lee.accumulation.database.data.Student;
import com.crazyice.lee.accumulation.database.data.User;
import com.crazyice.lee.accumulation.database.inter.StudentDao;
import com.crazyice.lee.accumulation.database.service.UserRepository;
import com.google.common.base.Charsets;
import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnel;
import com.google.common.hash.Funnels;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Random;
import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationTests {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationTests.class);

    @Autowired
    private StudentDao studentDao;
    @Autowired
    private UserRepository userRepository;

    @Test
    public void contextLoads() {
    }

    //@Test
    public void addStudent() {
        Random random = new Random(29);
        for (int i = 0; i < 100; i++) {
            Student student = new Student();
            student.setName("名字" + i);
            student.setAge(random.nextInt(29));
            student.setSex(random.nextInt(1) == 0 ? "女" : "男");
            student.setEmail("test" + random.nextInt(100) + "@ppp.com");
            studentDao.addStudent(student);
        }
    }

    //@Test
    public void searchStudent() {
        List<Student> students = studentDao.searchAge(20);
        LOGGER.info("{}", students);
    }

    @Test
    public void insertUser() {
        Random random = new Random(System.currentTimeMillis());
        for (int i = 0; i < 100; i++) {
            User user = new User();
            user.setAge(random.nextInt(80));
            user.setId(UUID.randomUUID().toString());
            user.setName("李征兵");
            user.setPassword(Integer.toString(random.nextInt()));
            user.setPhone("123124214234");
            user.setRegisterTime(new DateTime(System.currentTimeMillis()));
            user.setSex("男");
            user.setUsername("crayicelee");
            user.setHashcode(user.toString().hashCode());
            userRepository.save(user);
        }
    }

    //@Test
    public void bloomFilter() {
        int expectedInsertions = 10000000;
        double fpp = 0.00001;

        BloomFilter<CharSequence> bloomFilter = BloomFilter.create(Funnels.stringFunnel(Charsets.UTF_8), expectedInsertions, fpp);

        bloomFilter.put("aaa");
        bloomFilter.put("bbb");
        boolean containsString = bloomFilter.mightContain("aaa");
        LOGGER.info("{}", containsString);

        BloomFilter<Email> emailBloomFilter = BloomFilter
                .create((Funnel<Email>) (from, into) -> into.putString(from.getUsername(), Charsets.UTF_8), expectedInsertions, fpp);

        emailBloomFilter.put(new Email("sage.wang", "quanr.com"));
        boolean containsEmail = emailBloomFilter.mightContain(new Email("sage.wangaaa", "quanr.com"));
        LOGGER.info("{}", containsEmail);
    }
}
