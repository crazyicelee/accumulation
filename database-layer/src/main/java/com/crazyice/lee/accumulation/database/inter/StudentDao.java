package com.crazyice.lee.accumulation.database.inter;

import com.crazyice.lee.accumulation.database.data.Student;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface StudentDao {
    @Results({
            @Result(property = "name", column = "name"),
            @Result(property = "age", column = "age"),
            @Result(property = "sex", column = "sex"),
            @Result(property = "email", column = "email")
    })
    @Select("SELECT * FROM student WHERE age > #{age}")
    List<Student> searchAge(int age);

    @Insert("INSERT INTO student(name, age,sex,email) VALUES (#{name}, #{age},#{sex},#{email})")
    void addStudent(Student user);

    @Update("UPDATE student SET name=#{name},sex=#{sex},age=#{age} WHERE email=#{email}")
    void updateStudent(Student student);

    @Delete("DELETE FROM student WHERE email=#{email}")
    void deleteStudent(String email);
}
