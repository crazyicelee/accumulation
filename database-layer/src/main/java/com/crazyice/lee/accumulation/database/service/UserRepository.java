package com.crazyice.lee.accumulation.database.service;

import com.crazyice.lee.accumulation.database.data.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Service;

@Service
public interface UserRepository extends MongoRepository<User, String> {
}
