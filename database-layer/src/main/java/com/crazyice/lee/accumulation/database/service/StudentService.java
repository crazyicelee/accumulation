package com.crazyice.lee.accumulation.database.service;

import com.crazyice.lee.accumulation.database.data.Student;
import com.crazyice.lee.accumulation.database.inter.StudentDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentService {
    @Autowired
    private StudentDao studentDao;

    public List<Student> showStudent(int age) {
        return studentDao.searchAge(age);
    }

    public void addStudent(Student student) {
        studentDao.addStudent(student);
    }
}
