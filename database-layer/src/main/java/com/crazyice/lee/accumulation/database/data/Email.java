package com.crazyice.lee.accumulation.database.data;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Email {
    private String username;
    private String domain;
}
