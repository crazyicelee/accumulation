package com.crazyice.lee.accumulation.database.web;

import com.crazyice.lee.accumulation.database.data.Student;
import com.crazyice.lee.accumulation.database.service.StudentService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DatabaseController {
    private static Logger log = LoggerFactory.getLogger(DatabaseController.class);

    @Autowired
    private StudentService studentService;

    @RequestMapping(value = "/students", method = RequestMethod.GET)
    @ApiOperation(value = "获取学生列表", notes = "从数据库中获取年龄>指定数的信息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "age", value = "年龄", required = true, dataType = "Integer")
    })
    public List<Student> students(@RequestParam("age") Integer age) {
        return studentService.showStudent(age);
    }

}
