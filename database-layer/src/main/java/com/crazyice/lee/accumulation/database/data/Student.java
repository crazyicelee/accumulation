package com.crazyice.lee.accumulation.database.data;

import lombok.Data;

@Data
public class Student {
    private String name;
    private int age;
    private String sex;
    private String email;
}
