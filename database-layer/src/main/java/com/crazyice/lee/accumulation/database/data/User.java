package com.crazyice.lee.accumulation.database.data;

import lombok.Data;
import org.joda.time.DateTime;

@Data
public class User {
    private String id;
    private String username;
    private String password;
    private DateTime registerTime;
    private String phone;
    private String name;
    private String sex;
    private int age;
    private int hashcode;
}
