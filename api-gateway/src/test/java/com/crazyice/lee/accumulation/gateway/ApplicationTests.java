package com.crazyice.lee.accumulation.gateway;

import com.crazyice.lee.accumulation.gateway.utils.RSAUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.util.Base64;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationTests {

    @Test
    public void contextLoads() {
    }

    @Test
    public void testGetKeyPair() throws Exception {
        KeyPair keyPair = RSAUtils.getKeyPair(1024);
        PublicKey publicKey = RSAUtils.getPublicKey("MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDVLxO+NsqClXZmKS9QIL1TZEOR" +
                "Pn7EnbX5vHDKuluKQwl78Pknpxfrfg4rNypn1U/EIkdl9l53jvs1T7vWbtUp4FKp" +
                "akHnwM5JAnREVsYiIstVMwwBInK1RTgg6c87pCm9QoMUpUtqkdk4uxtYYSloDfwJ" +
                "ISoyuRowrSF6Q/EYkQIDAQAB");
        Security.addProvider(new BouncyCastleProvider());
        PrivateKey privateKey = RSAUtils.getPrivateKey("MIICXQIBAAKBgQDVLxO+NsqClXZmKS9QIL1TZEORPn7EnbX5vHDKuluKQwl78Pkn" +
                "pxfrfg4rNypn1U/EIkdl9l53jvs1T7vWbtUp4FKpakHnwM5JAnREVsYiIstVMwwB" +
                "InK1RTgg6c87pCm9QoMUpUtqkdk4uxtYYSloDfwJISoyuRowrSF6Q/EYkQIDAQAB" +
                "AoGBAJ2gw45f4PkRHoyHY05yBstEVZlthjpHCqjvps6fYNQooTidgvzI1izv9fYK" +
                "QjaoSOSTHAvOUSdB94kBvbfeF4oWqSLq957/iiPRuYuNdAK72a5bpL32vBXAEwo8" +
                "EvA8AfMziHiDL5fSMaaTZ21EQQcJtfYpu/D6suXg7RG4QKABAkEA/IP776diSdnu" +
                "zGb7APjeFI6r3Ji5g2Lsy4VN22ixiLaXjxTnm01zoFzuJDRbleyx5LFvOWgClXB4" +
                "p3nl4VQ3kQJBANggJzUhWjOyZKaiZ/iKE5ym4P3Zjy0RAZU9UeofLRSaJmamau+D" +
                "csFMJjda9GgC/nsLcG+aZ2J/rJyla7iIUQECQA7dbi1TezPBwo8B2PDbtaccJv3b" +
                "d8BGSI5KkKTD6v+TUCS6JFA+JPphvhsfgey3fFlsIJIOj2hgsJmbUW8MWCECQQCy" +
                "U51mNlXSECECaUC2j19tQzij3C5J9h1DQaxkLEMrG6IR0sCrTd+S5L0nsZG2fLnz" +
                "VtX4EAO7/zKysb83LKsBAkA3X6XTRx0avKGXypwtlx9VhfnwvRuot+moa9d8Qp5f" +
                "VodSJ4rJHc1oHZof71QNLaVvrV9nHy0kjHEDuuPr9xyP");
        //私钥加密，公钥解密
        String mingwen0 = "送一封信给上帝！send email to god!";
        String miwen = new String(Base64.getEncoder().encode(RSAUtils.encryptByPrivateKey(mingwen0.getBytes(), privateKey)));
        String mingwen1 = new String(RSAUtils.decrypByPublicKey(Base64.getDecoder().decode(miwen.getBytes()), publicKey));
        Assert.assertEquals(mingwen0, mingwen1);
        //公钥加密，私钥解密
        miwen = new String(Base64.getEncoder().encode(RSAUtils.encryptByPublicKey(mingwen0.getBytes(), publicKey)));
        mingwen1 = new String(RSAUtils.decryptByPrivateKey(Base64.getDecoder().decode(miwen.getBytes()), privateKey));
        Assert.assertEquals(mingwen0, mingwen1);
    }
}
