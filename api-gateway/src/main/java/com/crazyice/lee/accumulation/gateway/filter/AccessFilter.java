package com.crazyice.lee.accumulation.gateway.filter;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.annotation.JSONField;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AccessFilter extends ZuulFilter {

    private static Logger log = LoggerFactory.getLogger(AccessFilter.class);

    @Value("${jwtSecret}")
    private String jwtSecret;

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Data
    class ReturnResult {
        @JSONField
        public int code;
        @JSONField
        public String message;
    }

    @Override
    public Object run() {
        ReturnResult returnResult = new ReturnResult();

        RequestContext ctx = RequestContext.getCurrentContext();
        HttpServletRequest request = ctx.getRequest();

        Cookie[] cookies = request.getCookies();
        if (cookies == null) {
            ctx.setSendZuulResponse(false);
            ctx.setResponseStatusCode(401);
            returnResult.setCode(-1000);
            returnResult.setMessage("access token没有写入cookie！");
            ctx.setResponseBody(JSON.toJSONString(returnResult));
            return null;
        }

        String accessToken = null;
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals("accessToken")) {
                accessToken = cookie.getValue();
                break;
            }
        }
        if (accessToken == null) {
            log.warn("access token is empty");
            ctx.setSendZuulResponse(false);
            ctx.setResponseStatusCode(401);
            returnResult.setCode(-1000);
            returnResult.setMessage("access token 为空，非法访问!");
            ctx.setResponseBody(JSON.toJSONString(returnResult));
            return null;
        } else {
            try {
                //通过JWT解析token进行合法性验证
                Claims claims = Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(accessToken).getBody();

                //将解码后的数据传递给微服务
                Map<String, List<String>> requestQueryParams = ctx.getRequestQueryParams();
                if (requestQueryParams == null) {
                    requestQueryParams = new HashMap<>();
                } else {
                    //过滤掉accessToken
                    requestQueryParams.remove("accessToken");
                }
                ArrayList<String> nameList = new ArrayList<>();
                nameList.add(claims.get("username").toString());
                ArrayList<String> userIdList = new ArrayList<>();
                userIdList.add(claims.get("id").toString());
                requestQueryParams.put("username", nameList);
                requestQueryParams.put("id", userIdList);
                ctx.setRequestQueryParams(requestQueryParams);
            } catch (Exception e) {
                log.error(e.getLocalizedMessage());
                log.error("access fail！");
                ctx.setSendZuulResponse(false);
                ctx.setResponseStatusCode(403);
                returnResult.setCode(-2000);
                returnResult.setMessage("access token fail!");
                ctx.setResponseBody(JSON.toJSONString(returnResult));
                return null;
            }
        }
        log.info("access token ok");
        return null;
    }

}
