package com.crazyice.lee.accumulation.cache.conf;

import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.HostAndPort;
import redis.clients.jedis.JedisCluster;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.util.HashSet;
import java.util.Set;

/**
 * redis的配置参数bean，动态从consul获取
 **/
@Configuration
@ConfigurationProperties(prefix = "redis")
@Data
public class RedisConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(RedisConfig.class);

    private String host;
    private int port;
    private String password;
    private int timeout;
    private int maxActive;
    private int maxIdle;
    private int minIdle;
    private long maxWaitMillis;
    private int expireSeconds;
    private String clusterNodes;
    private int commandTimeout;

    /**
     * 返回JedisPool的单例，可以注入到其他类中直接使用
     *
     * @return
     */
    @Bean
    public JedisPool redisPoolFactory() {
        JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
        jedisPoolConfig.setMaxIdle(maxIdle);
        jedisPoolConfig.setMaxWaitMillis(maxWaitMillis);
        jedisPoolConfig.setMaxTotal(maxActive);
        jedisPoolConfig.setMinIdle(minIdle);

        JedisPool jedisPool;
        if (password.isEmpty()) {
            jedisPool = new JedisPool(jedisPoolConfig, host, port, timeout, null);
        } else {
            jedisPool = new JedisPool(jedisPoolConfig, host, port, timeout, password);
        }
        LOGGER.info("JedisPool注入成功！");
        return jedisPool;
    }

    /**
     * 注意：
     * 这里返回的JedisCluster是单例的，并且可以直接注入到其他类中去使用
     *
     * @return
     */
    //@Bean
    public JedisCluster jedisClusterFactory() {
        String[] serverArray = clusterNodes.split(",");
        Set<HostAndPort> nodes = new HashSet<>();

        for (String ipPort : serverArray) {
            String[] ipPortPair = ipPort.split(":");
            nodes.add(new HostAndPort(ipPortPair[0].trim(), Integer.valueOf(ipPortPair[1].trim())));
            LOGGER.info("redis:" + ipPortPair);
        }
        JedisCluster jedisCluster = new JedisCluster(nodes, commandTimeout);
        LOGGER.info("JedisCluster注入成功！");
        return jedisCluster;
    }
}
