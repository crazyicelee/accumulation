package com.crazyice.lee.accumulation.cache.web;

import com.crazyice.lee.accumulation.cache.util.JedisUtil;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.Transaction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.LongAdder;

@RestController
public class RedisController {
    private static Logger log = LoggerFactory.getLogger(RedisController.class);

    private static final int TASK_COUNT = 5;//任务数
    private static final int TARGET_COUNT = 100;

    private LongAdder lacount = new LongAdder();
    private static CountDownLatch cdladdr = new CountDownLatch(TASK_COUNT);
    private Long test = 0L;

    @Autowired
    private JedisUtil jedisUtil;

    @Autowired
    private JedisPool jedisPool;

    /**
     * 快速原子计数类LongAdder的使用
     */
    public class LongAdderThread implements Runnable {
        protected int threadID;
        protected long starttime;

        public LongAdderThread(int threadID, long starttime) {
            this.threadID = threadID;
            this.starttime = starttime;
        }

        @Override
        public void run() {
            long v = lacount.sum();
            while (v < TARGET_COUNT) {
                lacount.increment();
                v = lacount.sum();
                log.info("{}:{}", threadID, v);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            long endtime = System.currentTimeMillis();
            log.info("LongAdderThread {}用时:{}，计数值:{}", threadID, (endtime - starttime), test);
            cdladdr.countDown();
        }

    }

    /**
     * redis事务处理示例
     */
    class MyTask implements Runnable {
        private int taskNum;
        private String id;

        public MyTask(int num, String id) {
            this.taskNum = num;
            this.id = id;
        }

        @Override
        public void run() {
            log.info("线程{}开始执行...", taskNum);
            Jedis jedis = jedisPool.getResource();
            try {
                //执行redis的原子操作or执行事务操作
                if (false) {
                    log.info("执行结果：{}", jedisUtil.decr(id));
                } else {
                    Transaction transaction = jedis.multi();
                    transaction.decr(id);
                    transaction.rpop("product");
                    List<Object> ret = transaction.exec();
                    log.info("执行结果：{}", ret);
                }
            } catch (Exception e) {
                log.error(e.getLocalizedMessage());
            } finally {
                try {
                    jedis.close();
                } catch (Exception e) {
                    log.error(e.getLocalizedMessage());
                }
                log.info("线程{}执行结束.", taskNum);
            }
        }
    }

    /**
     * Collatz猜想验证
     */
    class Collatz implements Runnable {
        private long number;

        public Collatz(long number) {
            this.number = number;
        }

        @Override
        public void run() {
            Long value = number;
            List<Long> proccessQueue = new ArrayList();
            proccessQueue.add(number);
            while (number != 1) {
                if (number % 2 == 1) {
                    number = number * 3 + 1;
                } else {
                    number = number / 2;
                }
                proccessQueue.add(number);
            }
            Collections.reverse(proccessQueue);
            log.info("{}:{}:{}", value, proccessQueue.size(), proccessQueue);
        }
    }

    @RequestMapping(value = "/seckill", method = RequestMethod.GET)
    @ApiOperation(value = "秒杀", notes = "（多线程并发）将给定ID的商品数量递减150")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "id", value = "商品ID", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "query", name = "num", value = "商品库存数量", required = true, dataType = "String")
    })
    public String secondKill(@RequestParam("id") String id, @RequestParam("num") String num) {
        jedisUtil.set(id, num, 0);

        //将待秒杀的商品货号压入堆栈
        String[] productNO = new String[TASK_COUNT];
        for (int i = 0; i < TASK_COUNT; i++) {
            productNO[i] = "productno:" + i;
        }
        jedisUtil.rpush("product", productNO);

        //启用线程池
        int npus = Runtime.getRuntime().availableProcessors();
        log.info("CPU数量{}", npus);
        ExecutorService executor = Executors.newFixedThreadPool(npus);
        for (int i = 0; i < TASK_COUNT; i++) {
            MyTask myTask = new MyTask(i, id);
            executor.execute(myTask);
        }
        executor.shutdown();
        while (true) {
            if (executor.isTerminated()) {
                break;
            } else {
                try {
                    Thread.sleep(1000);
                } catch (Exception e) {
                    log.error(e.getLocalizedMessage());
                }
            }
        }
        return jedisUtil.get(id, 0);
    }

    @RequestMapping(value = "/collatz", method = RequestMethod.GET)
    public Boolean callCollatz(@RequestParam("num") long num) throws InterruptedException {
        //启用线程池
        int npus = Runtime.getRuntime().availableProcessors();
        ExecutorService executor = Executors.newFixedThreadPool(npus);
        for (long i = num; i > 0; i--) {
            Collatz collatz = new Collatz(i);
            executor.execute(collatz);
        }
        executor.shutdown();
        return true;
    }

    @RequestMapping(value = "/longadder", method = RequestMethod.GET)
    public Boolean callLongAdder() throws InterruptedException {
        long starttime = System.currentTimeMillis();
        //启用线程池
        int npus = Runtime.getRuntime().availableProcessors();
        ExecutorService executor = Executors.newFixedThreadPool(npus);
        for (int i = 0; i < TASK_COUNT; i++) {
            LongAdderThread longAdderThread = new LongAdderThread(i, starttime);
            executor.submit(longAdderThread);
        }
        cdladdr.await();
        executor.shutdown();
        return true;
    }
}
