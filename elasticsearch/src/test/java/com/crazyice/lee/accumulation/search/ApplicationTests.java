package com.crazyice.lee.accumulation.search;

import com.crazyice.lee.accumulation.search.data.Article;
import com.crazyice.lee.accumulation.search.data.FactorialData;
import com.crazyice.lee.accumulation.search.service.RecursionFile;
import com.crazyice.lee.accumulation.search.service.RecursionFactor;
import io.searchbox.client.JestClient;
import io.searchbox.core.Index;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.text.PDFTextStripper;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ApplicationTests {
    @Autowired
    ApplicationContext applicationContext;

    @Autowired
    JestClient jestClient;

    @Autowired
    RecursionFile directoryRecurse;

    private String indexName = "diskfile";
    private String typeName = "files";

    @Test
    public void contextLoads() {
    }

    //@Test
    public void search() {
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.termQuery("content", "中国"));

        HighlightBuilder highlightBuilder = new HighlightBuilder();
        //path属性高亮度
        HighlightBuilder.Field highlightPath = new HighlightBuilder.Field("path");
        highlightPath.highlighterType("unified");
        highlightBuilder.field(highlightPath);
        //title字段高亮度
        HighlightBuilder.Field highlightTitle = new HighlightBuilder.Field("title");
        highlightTitle.highlighterType("unified");
        highlightBuilder.field(highlightTitle);
        //content字段高亮度
        HighlightBuilder.Field highlightContent = new HighlightBuilder.Field("content");
        highlightContent.highlighterType("unified");
        highlightBuilder.field(highlightContent);

        //高亮度配置生效
        searchSourceBuilder.highlighter(highlightBuilder);

        log.info("搜索条件{}", searchSourceBuilder.toString());

        //构建搜索功能
        Search search = new Search.Builder(searchSourceBuilder.toString()).addIndex(indexName).addType(typeName).build();
        try {
            //执行
            SearchResult result = jestClient.execute(search);
            result.getHits(Article.class).forEach((value) -> {
                log.info("{}", value.highlight);
            });
        } catch (IOException e) {
            log.error("{}", e.getLocalizedMessage());
        }
    }

    //@Test
    public void listFiles() throws IOException{
        directoryRecurse.filterFile("/Users/crazyicelee");
        directoryRecurse.doneFile("ext",true);
        //fileTypes频率排序
        List<Map.Entry<String,Integer>> list = new ArrayList<>(directoryRecurse.getFileTypes().entrySet());
        //降序排序
        Collections.sort(list, (o1, o2) -> o2.getValue().compareTo(o1.getValue()));

        log.info("文件类型：{}",list);
    }

    //@Test
    public void addIndex() {
        Article article = new Article();
        article.setId("2");
        article.setTitle("中华人民共和国");
        article.setAuthor("我是中国人");
        article.setPath("中国人民卫生");
        article.setContent("中国人民解放军");
        //2. 构建一个索引
        Index index = new Index.Builder(article).index(indexName).type(typeName).build();
        try {
            //3. 执行
            jestClient.execute(index);
            log.info("构建索引成功！");
        } catch (IOException e) {
            log.error("{}", e.getLocalizedMessage());
        }

    }

    //@Test
    public void getLocalIpAddr() {

        String clientIP = null;
        Enumeration<NetworkInterface> networks = null;
        try {
            //获取所有网卡设备
            networks = NetworkInterface.getNetworkInterfaces();
            if (networks == null) {
                //没有网卡设备 打印日志  返回null结束
                log.info("networks  is null");
                return;
            }
        } catch (SocketException e) {
            log.error("{}", e.getLocalizedMessage());
        }
        InetAddress ip;
        Enumeration<InetAddress> addrs;
        // 遍历网卡设备
        while (networks.hasMoreElements()) {
            NetworkInterface ni = networks.nextElement();
            try {
                //过滤掉 loopback设备、虚拟网卡
                if (!ni.isUp() || ni.isLoopback() || ni.isVirtual()) {
                    continue;
                }
            } catch (SocketException e) {
                log.error("{}", e.getLocalizedMessage());
            }
            addrs = ni.getInetAddresses();
            if (addrs == null) {
                log.info("InetAddress is null");
                continue;
            }
            // 遍历InetAddress信息
            while (addrs.hasMoreElements()) {
                ip = addrs.nextElement();
                if (!ip.isLoopbackAddress() && ip.isSiteLocalAddress() && ip.getHostAddress().indexOf(":") == -1) {
                    try {
                        clientIP = ip.toString().split("/")[1];
                    } catch (ArrayIndexOutOfBoundsException e) {
                        clientIP = null;
                    }
                }
            }
        }
        log.info("{}", clientIP);
    }

    //@Test
    public void readPdf() {
        File file = new File("/Users/crazyicelee/Documents/企通集团/史记.pdf");
        try (PDDocument document = PDDocument.load(file)) {
            PDFTextStripper stripper = new PDFTextStripper();
            stripper.setSortByPosition(true);
            for (int i = 0; i < document.getNumberOfPages(); i++) {
                PDPage page = document.getPage(i);
                stripper.setStartPage(i);
                stripper.setEndPage(i + 1);
                log.info("第{}页", i);
                log.info("{}", stripper.getText(document));
            }
        } catch (Exception e) {
            log.error("{}", e.getLocalizedMessage());
        }

    }

    //@Test
    public void factorTest() {
        FactorialData factorialData = new FactorialData();
        factorialData.setBegin(1L);
        factorialData.setEnd(9_000L);
        factorialData.setTotal(new BigDecimal(1L));
        log.info("{}", RecursionFactor.recursion(factorialData).invoke());
    }

    @Test
    public void allBean(){
        String[] beans=applicationContext.getBeanDefinitionNames();
        log.info("已经注册的bean");
        Arrays.stream(beans).forEach(bean-> log.info("{} type:{}",bean,applicationContext.getBean(bean).getClass()));
    }

}
