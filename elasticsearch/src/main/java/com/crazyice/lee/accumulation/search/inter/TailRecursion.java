package com.crazyice.lee.accumulation.search.inter;

import java.util.stream.Stream;

//尾调用的接口，定义了是否完成，执行等方法
public interface TailRecursion<T> {
    TailRecursion<T> apply();
    default Boolean isFinished() {
        return Boolean.FALSE;
    }
    default T getResult() {
        throw new RuntimeException("递归还没有结束，暂时得不到结果");
    }
    default T invoke() {
        return Stream.iterate(this, TailRecursion::apply)
                .filter(TailRecursion::isFinished)
                .findFirst()
                .get()
                .getResult();
    }
}