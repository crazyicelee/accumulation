package com.crazyice.lee.accumulation.search.inter;

import com.crazyice.lee.accumulation.search.data.Article;
import com.crazyice.lee.accumulation.search.utils.Md5CaculateUtil;

import java.io.File;

public abstract class ReadFileContent {
    public Article Read(File file, String serviceIP){
        Article article = new Article();
        article.setTitle(file.getName());
        article.setAuthor(file.getParent());
        article.setPath("file://" + serviceIP + ":" + file.toURI().getPath());
        article.setContent(readToString(file));
        article.setFileFingerprint(Md5CaculateUtil.getMD5(file));
        return article;
    }

    public String charFilter(String s){
        if (s.length() > 0) {
            //替换\n、\t、\r等为网页标签
            return s.toString().replaceAll("(\r\n|\r|\n|\n\r)+", "<br>").replaceAll("\t", "&nbsp;");
        } else {
            return "";
        }
    }

    //读取文件内容
    public abstract String readToString(File file);

}
