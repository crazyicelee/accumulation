package com.crazyice.lee.accumulation.search.service;

import com.crazyice.lee.accumulation.search.data.Article;
import com.crazyice.lee.accumulation.search.impl.DocFile;
import com.crazyice.lee.accumulation.search.impl.DocxFile;
import com.crazyice.lee.accumulation.search.impl.PdfFile;
import com.crazyice.lee.accumulation.search.impl.TxtFile;
import com.crazyice.lee.accumulation.search.inter.ReadFileContent;
import io.searchbox.client.JestClient;
import io.searchbox.core.Index;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import net.sf.jmimemagic.*;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;

@Component
@Slf4j
@Data
public class RecursionFile {
    //可处理文件类型
    private static List<String> FILETYPE=Arrays.asList("text/palin","txt","java","h","c","cpp","doc","docx","pdf");

    //判断文件类型的方法
    private static String JUDGE="ext";

    @Autowired
    private JestClient jestClient;

    //代理服务器IP
    private String serviceIp;
    //文件类型列表
    private Map<String,Integer> fileTypes=new HashMap<>();
    //待处理文件File列表
    private List<File> destFile=new ArrayList<>();

    public RecursionFile() {
        serviceIp = getLocalIpAddr();
    }

    //获取服务器ip
    private static String getLocalIpAddr() {
        String clientIP = null;
        Enumeration<NetworkInterface> networks = null;
        try {
            //获取所有网卡设备
            networks = NetworkInterface.getNetworkInterfaces();
            if (networks == null) {
                //没有网卡设备 打印日志  返回null结束
                log.info("networks  is null");
                return "";
            }
        } catch (SocketException e) {
            log.error("{}", e.getLocalizedMessage());
        }
        InetAddress ip;
        Enumeration<InetAddress> addrs;
        // 遍历网卡设备
        while (networks.hasMoreElements()) {
            NetworkInterface ni = networks.nextElement();
            try {
                //过滤掉 loopback设备、虚拟网卡
                if (!ni.isUp() || ni.isLoopback() || ni.isVirtual()) {
                    continue;
                }
            } catch (SocketException e) {
                log.error("{}", e.getLocalizedMessage());
            }
            addrs = ni.getInetAddresses();
            if (addrs == null) {
                log.info("InetAddress is null");
                continue;
            }
            // 遍历InetAddress信息
            while (addrs.hasMoreElements()) {
                ip = addrs.nextElement();
                if (!ip.isLoopbackAddress() && ip.isSiteLocalAddress() && ip.getHostAddress().indexOf(":") == -1) {
                    try {
                        clientIP = ip.toString().split("/")[1];
                    } catch (ArrayIndexOutOfBoundsException e) {
                        clientIP = null;
                    }
                }
            }
        }
        log.info("{}", clientIP);
        return clientIP;
    }

    //判断是否已经索引
    private Boolean isIndex(Article article) {
        //用MD5生成文件指纹,搜索该指纹是否已经索引
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.termQuery("fileFingerprint", article.getFileFingerprint()));
        Search search = new Search.Builder(searchSourceBuilder.toString()).addIndex("diskfile").addType("files").build();
        try {
            //执行
            SearchResult searchResult = jestClient.execute(search);
            if (searchResult.getTotal() > 0) {
                return true;
            } else {
                return false;
            }
        } catch (IOException e) {
            log.error("{}", e.getLocalizedMessage());
        }
        return false;
    }

    //判断文件类型
    private String fileType(File file,String method){
        String fileType = null;
        switch (method) {
            case "magic":
                Magic parser = new Magic();
                try {
                    MagicMatch match = parser.getMagicMatch(file, false);
                    fileType = match.getMimeType();
                } catch (MagicParseException e) {
                    //log.error("{}",e.getLocalizedMessage());
                } catch (MagicMatchNotFoundException e) {
                    //log.error("{}",e.getLocalizedMessage());
                } catch (MagicException e) {
                    //log.error("{}",e.getLocalizedMessage());
                }
                break;
            case "ext":
                String filename = file.getName();
                String[] strArray = filename.split("\\.");
                int suffixIndex = strArray.length - 1;
                fileType = strArray[suffixIndex];
                break;
        }
        //判断文件是否为link类型
        try {
            if(!file.getCanonicalFile().equals(file.getAbsoluteFile())){
                fileType="link";
            }
        } catch (IOException e) {
            log.error("{}",e.getLocalizedMessage());
        }
        return fileType;
    }

    //对文件目录及内容创建索引
    private void createIndex(File file, String method,Boolean onlyFileType) {
        String fileType = fileType(file,method);
        if(fileType.length()>5) return;

        //收集文件类型及频率
        synchronized(this){
            if (fileTypes.containsKey(fileType))
                fileTypes.put(fileType, fileTypes.get(fileType) + 1);
            else
                fileTypes.put(fileType, 1);
        }
        if(onlyFileType) return;

        //1. 给ES中索引(保存)一个文档
        ReadFileContent readFileContent=null;
        switch (fileType) {
            case "text/plain":
            case "java":
            case "c":
            case "cpp":
            case "txt":
                readFileContent = new TxtFile();
                break;
            case "doc":
                readFileContent = new DocFile();
                break;
            case "docx":
                readFileContent = new DocxFile();
                break;
            case "pdf":
                readFileContent = new PdfFile();
                break;
        }
        if(readFileContent==null) return;
        Article article = readFileContent.Read(file, serviceIp);

        //是否已经被索引
        Boolean isIndexResult = isIndex(article);
        log.info("文件名：{}，文件类型：{}，MD5：{}，建立索引：{}", file.getPath(), fileType, article.getFileFingerprint(), isIndexResult);
        if (isIndexResult) return;

        //2. 构建一个索引
        Index index = new Index.Builder(article).index("diskfile").type("files").build();
        try {
            //3. 执行
            if (!jestClient.execute(index).getId().isEmpty()) {
                log.info("构建索引成功！");
            }
        } catch (IOException e) {
            log.error("{}", e.getLocalizedMessage());
        }
    }

    //流方式并行处理文件
    public void doneFile(String method,Boolean onlyFileType){
        destFile.parallelStream().forEach(file -> createIndex(file,method,onlyFileType));
    }

    //遍历指定目录下的全部文件
    @Deprecated
    public void find(String pathName) {
        //获取pathName的File对象
        File dirFile = new File(pathName);

        //判断是否有读权限
        if (!dirFile.canRead()){
            log.info("do not read");
            return;
        }

        if (!dirFile.isDirectory()) {
            String fileType=fileType(dirFile,JUDGE);
            if(FILETYPE.contains(fileType)) {
                destFile.add(dirFile);
            }
        }
        else {
            //获取此目录下的所有文件名与目录名
            String[] fileList = dirFile.list();
            for (String subFile : fileList) {
                File file = new File(dirFile.getPath(), subFile);
                if (!file.canRead()) {
                    continue;
                }
                //如果是一个目录，输出目录名后，进行递归
                if (file.isDirectory()) {
                    if(fileType(file, JUDGE).equals("link")) continue;
                    //递归
                    try {
                        find(file.getCanonicalPath());
                    } catch (Exception e) {
                        log.error("{}", e.getLocalizedMessage());
                    }
                }
                else {
                    //忽略掉临时文件，以~$起始的文件名
                    if (file.getName().startsWith("~$")) continue;
                    if (FILETYPE.contains(fileType(file, JUDGE))) {
                        destFile.add(file);
                    }
                }
            }
        }
    }

    //利用File的walkFileTree方法实现文件遍历
    public void filterFile(String pathName) throws IOException{
        Files.walkFileTree(Paths.get(pathName), new SimpleFileVisitor<Path>() {

            @Override
            public FileVisitResult visitFile(Path path, BasicFileAttributes attrs) {
                if(attrs.isRegularFile() && !path.startsWith("~$")) {
                    File file=path.toFile();
                    String fileType=fileType(file,JUDGE);
                    if(FILETYPE.contains(fileType)) {
                        destFile.add(file);
                    }
                }
                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFileFailed(Path file, IOException exc){
                return FileVisitResult.CONTINUE;
            }
        });
    }
}