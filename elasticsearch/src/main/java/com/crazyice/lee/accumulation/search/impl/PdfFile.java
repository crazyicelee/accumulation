package com.crazyice.lee.accumulation.search.impl;

import com.crazyice.lee.accumulation.search.inter.ReadFileContent;
import lombok.extern.slf4j.Slf4j;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.File;

@Slf4j
public class PdfFile extends ReadFileContent {
    public String readToString(File file){
        StringBuffer result = new StringBuffer();
        try (PDDocument document = PDDocument.load(file)) {
            PDFTextStripper stripper = new PDFTextStripper();
            stripper.setSortByPosition(true);
            int pages = document.getNumberOfPages();
            for (int i = 0; i < pages; i++) {
                stripper.setStartPage(i);
                stripper.setEndPage(i + 1);
                result.append(stripper.getText(document));
            }
        } catch (Exception e) {
            log.error("{}", e.getLocalizedMessage());
        }
        return charFilter(result.toString());
    }
}
