package com.crazyice.lee.accumulation.search.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Configuration
@Component
@Slf4j
public class CreateIndexTask {
    @Autowired
    private RecursionFile recursionFile;

    @Value("${index-root}")
    private String indexRoot;

    @Scheduled(cron = "0 5/59 * * * ?")
    private void addIndex() throws IOException {
        log.info("根目录：{}", indexRoot);
        recursionFile.filterFile(indexRoot);
        recursionFile.doneFile("ext",false);
        //fileTypes频率排序
        List<Map.Entry<String,Integer>> list = new ArrayList<>(recursionFile.getFileTypes().entrySet());
        //降序排序
        Collections.sort(list, (o1, o2) -> o2.getValue().compareTo(o1.getValue()));
        log.info("文件类型：{}",list);
        //清理空间
        recursionFile.getDestFile().clear();
        recursionFile.getFileTypes().clear();
    }
}
