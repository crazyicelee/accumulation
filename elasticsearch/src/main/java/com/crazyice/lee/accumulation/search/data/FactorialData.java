package com.crazyice.lee.accumulation.search.data;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class FactorialData {
    private Long begin;
    private Long end;
    private BigDecimal total;
}