package com.crazyice.lee.accumulation.search.impl;

import com.crazyice.lee.accumulation.search.inter.ReadFileContent;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.openxml4j.util.ZipSecureFile;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.File;
import java.io.FileInputStream;

@Slf4j
public class DocxFile extends ReadFileContent {
    public String readToString(File file){
        StringBuffer result = new StringBuffer();
        try (FileInputStream in = new FileInputStream(file); XWPFDocument doc = new XWPFDocument(in)) {
            ZipSecureFile.setMinInflateRatio(-1.0d);
            XWPFWordExtractor extractor = new XWPFWordExtractor(doc);
            result.append(extractor.getText());
        } catch (Exception e) {
            log.error("{}", e.getLocalizedMessage());
        }
        return charFilter(result.toString());
    }
}
