package com.crazyice.lee.accumulation.search.service;

import com.crazyice.lee.accumulation.search.data.FactorialData;
import com.crazyice.lee.accumulation.search.inter.TailRecursion;
import com.crazyice.lee.accumulation.search.utils.TailInvoke;

import java.math.BigDecimal;

public class RecursionFactor {
    public static TailRecursion<BigDecimal> recursion(FactorialData factorDTO) {
        // 如果已经递归到最后一个数字了，结束递归，返回 factorDTO.getTotal() 值
        if (factorDTO.getBegin().equals(factorDTO.getEnd())) {
            return TailInvoke.done(factorDTO.getTotal());
        }
        factorDTO.setBegin(1+factorDTO.getBegin());
        // 计算本次递归的值
        factorDTO.setTotal(factorDTO.getTotal().multiply(new BigDecimal(factorDTO.getBegin())));
        // 这里是最大的不同，这里每次调用递归方法时，使用的是 Lambda 的方式，这样只是初始化了一个 Lambda 变量而已，recursion1 方法的内存是不会分配的
        return TailInvoke.call(()->recursion(factorDTO));
    }
}
