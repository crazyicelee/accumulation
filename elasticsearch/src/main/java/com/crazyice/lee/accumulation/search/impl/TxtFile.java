package com.crazyice.lee.accumulation.search.impl;

import com.crazyice.lee.accumulation.search.inter.ReadFileContent;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

@Slf4j
public class TxtFile extends ReadFileContent {
     public String readToString(File file){
        StringBuffer result = new StringBuffer();
        try (FileInputStream in = new FileInputStream(file)) {
            byte[] buffer = new byte[8192];
            int length;
            while ((length = in.read(buffer)) != -1) {
                result.append(new String(buffer, 0, length, "utf8"));
            }
        } catch (FileNotFoundException e) {
            log.error("{}", e.getLocalizedMessage());
        } catch (IOException e) {
            log.error("{}", e.getLocalizedMessage());
        }
         return charFilter(result.toString());
    }
}
