package com.crazyice.lee.accumulation.search.data;

import io.searchbox.annotations.JestId;
import lombok.Data;

@Data
public class Article {
    @JestId
    private String id;
    private String fileFingerprint;
    private String author;
    private String title;
    private String path;
    private String content;
    private String highlightContent;
}
