package com.crazyice.lee.accumulation.search.web;

import com.crazyice.lee.accumulation.search.data.Article;
import io.searchbox.client.JestClient;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
@Slf4j
class SearchController {
    @Autowired
    private JestClient jestClient;

    @GetMapping("/")
    public String index() {
        return "index";
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String search(@RequestParam String keyword, Model model) {
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.queryStringQuery(keyword));

        HighlightBuilder highlightBuilder = new HighlightBuilder();
        //path属性高亮度
        HighlightBuilder.Field highlightPath = new HighlightBuilder.Field("path");
        highlightPath.highlighterType("unified");
        highlightBuilder.field(highlightPath);
        //title字段高亮度
        HighlightBuilder.Field highlightTitle = new HighlightBuilder.Field("title");
        highlightTitle.highlighterType("unified");
        highlightBuilder.field(highlightTitle);
        //content字段高亮度
        HighlightBuilder.Field highlightContent = new HighlightBuilder.Field("content");
        highlightContent.highlighterType("unified");
        highlightBuilder.field(highlightContent);

        //高亮度配置生效
        searchSourceBuilder.highlighter(highlightBuilder);

        log.info("搜索条件{}", searchSourceBuilder.toString());

        //构建搜索功能
        Search search = new Search.Builder(searchSourceBuilder.toString()).addIndex("diskfile").addType("files").build();
        try {
            //执行
            SearchResult result = jestClient.execute(search);
            List<Article> articles = new ArrayList<>();
            result.getHits(Article.class).forEach((value) -> {
                if (value.highlight != null && value.highlight.get("content") != null) {
                    StringBuffer highlightContentBuffer = new StringBuffer();
                    value.highlight.get("content").forEach(v -> {
                        highlightContentBuffer.append(v);
                    });
                    value.source.setHighlightContent(highlightContentBuffer.toString());
                }
                value.source.setContent(value.source.getContent());
                articles.add(value.source);
            });
            model.addAttribute("articles", articles);
            model.addAttribute("keyword", keyword);
            return "search";
        } catch (IOException e) {
            log.error("{}", e.getLocalizedMessage());
        }
        return "search";
    }
}
