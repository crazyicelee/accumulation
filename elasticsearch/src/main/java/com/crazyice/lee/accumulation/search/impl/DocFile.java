package com.crazyice.lee.accumulation.search.impl;

import com.crazyice.lee.accumulation.search.inter.ReadFileContent;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hwpf.extractor.WordExtractor;
import org.apache.poi.openxml4j.util.ZipSecureFile;

import java.io.File;
import java.io.FileInputStream;

@Slf4j
public class DocFile extends ReadFileContent {
    public String readToString(File file){
        StringBuffer result = new StringBuffer();
        //使用HWPF组件中WordExtractor类从Word文档中提取文本或段落
        try (FileInputStream in = new FileInputStream(file)) {
            ZipSecureFile.setMinInflateRatio(-1.0d);
            WordExtractor extractor = new WordExtractor(in);
            result.append(extractor.getText());
        } catch (Exception e) {
            log.error("{}", e.getLocalizedMessage());
        }

        return charFilter(result.toString());
    }
}
