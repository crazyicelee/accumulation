package com.crazyice.lee.accumulation.dictionary.web;

import com.crazyice.lee.accumulation.dictionary.conf.DictionaryConfig;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.InvocationTargetException;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@Slf4j
public class Controller {
    @Autowired
    private DictionaryConfig dictionaryConfig;

    //首字母转大写
    public static String toUpperCaseFirstOne(String s) {
        if (Character.isUpperCase(s.charAt(0)))
            return s;
        else
            return (new StringBuilder()).append(Character.toUpperCase(s.charAt(0))).append(s.substring(1)).toString();
    }

    @RequestMapping(value = "/getDictionary/{name}", method = RequestMethod.GET)
    @ApiOperation(value = "字典", notes = "获取指定字典的KV列表")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "filter", value = "删选key", dataType = "String")
    })
    public Map getDictionary(@PathVariable("name") String name, @RequestParam(value = "filter", name = "filter", required = false) String filter) {
        try {
            Map<String, String> totalMap = (Map) dictionaryConfig.getClass().getMethod("get" + toUpperCaseFirstOne(name)).invoke(dictionaryConfig);
            if (filter == null) {
                return totalMap;
            } else {
                return totalMap.entrySet().stream().filter((e) -> {
                    if (e.getKey().startsWith(filter))
                        return true;
                    else
                        return false;
                }).collect(Collectors.toMap((e) -> e.getKey(), (e) -> e.getValue()));
            }
        } catch (IllegalAccessException e) {
            log.error(e.getLocalizedMessage());
        } catch (InvocationTargetException e) {
            log.error(e.getLocalizedMessage());
        } catch (NoSuchMethodException e) {
            log.error(e.getLocalizedMessage());
        }
        return null;
    }
}
