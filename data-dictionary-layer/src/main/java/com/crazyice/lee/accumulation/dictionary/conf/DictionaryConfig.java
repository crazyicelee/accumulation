package com.crazyice.lee.accumulation.dictionary.conf;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

/**
 * 数据字典bean，动态从consul获取
 **/
@Configuration
@ConfigurationProperties(prefix = "dictionary")
@Data
public class DictionaryConfig {
    private Map sex;
    private Map province;
    private Map city;
    private Map flink;
}
