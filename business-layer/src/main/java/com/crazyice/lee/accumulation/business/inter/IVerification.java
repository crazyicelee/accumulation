package com.crazyice.lee.accumulation.business.inter;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;

@FeignClient(name = "accumulation-graphic-verification-layer")
public interface IVerification {
    @RequestMapping(value = "/getVerifier", method = RequestMethod.GET)
    Map<String, String> getVerifier();
}
