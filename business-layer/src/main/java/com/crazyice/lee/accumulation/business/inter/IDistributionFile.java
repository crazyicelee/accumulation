package com.crazyice.lee.accumulation.business.inter;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@FeignClient(name = "accumulation-distribution-file-layer")
@RequestMapping("/file")
public interface IDistributionFile {
    @PostMapping(value = "/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    String save(@RequestPart(value = "file") MultipartFile file);

    @RequestMapping(value = "/download/{fileName}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
    byte[] get(@PathVariable("fileName") String fileName);
}
