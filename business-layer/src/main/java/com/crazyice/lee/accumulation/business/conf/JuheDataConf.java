package com.crazyice.lee.accumulation.business.conf;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "juhe.data")
@Data
public class JuheDataConf {
    private String movieKey;
    private String idCardKey;
}
