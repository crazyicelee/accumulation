package com.crazyice.lee.accumulation.business.web;

import com.alibaba.fastjson.JSONObject;
import com.crazyice.lee.accumulation.business.inter.IDictionary;
import com.crazyice.lee.accumulation.business.inter.IDistributionFile;
import com.crazyice.lee.accumulation.business.inter.IVerification;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.Map;

@RestController
@CrossOrigin
public class DemoController {
    private static final Logger LOGGER = LoggerFactory.getLogger(DemoController.class);

    private static final String VERIFIER_CODE = "verifier_code";

    @Autowired
    private IDictionary dictionary;

    @Autowired
    private IDistributionFile distributionFile;

    @Autowired
    private IVerification verification;

    @RequestMapping(value = "/getDictionary/{name}", method = RequestMethod.GET)
    @ApiOperation(value = "数据字典", notes = "获取指定字典的KV列表")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "filter", value = "删选key", dataType = "String")
    })
    public Map getDataDictionary(@PathVariable("name") String name, @RequestParam(value = "filter", name = "filter", required = false) String filter) {
        return dictionary.getDataDictionary(name, filter);
    }

    @RequestMapping(value = "/download/{fileName}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
    @ApiOperation(value = "文件下载", notes = "从分布式文件存储服务器下载文件")
    public byte[] downloadFile(@PathVariable("fileName") String fileName) {
        return distributionFile.get(fileName);
    }

    @PostMapping(value = "/upload")
    @ApiOperation(value = "文件上传", notes = "将本地文件上传到分布式文件存储服务器")
    public String uploadFile(@RequestPart("file") MultipartFile file) {
        return distributionFile.save(file);
    }

    @RequestMapping(value = "/verifier", method = RequestMethod.GET)
    @ApiOperation(value = "生成图形验证码", notes = "生成图形验证码")
    public void verification(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setDateHeader("Expires", 0);
        response.setHeader("Cache-Control", "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        response.setContentType("image/jpeg");
        Map<String, String> value = verification.getVerifier();
        request.getSession().setAttribute(VERIFIER_CODE, value.get("capText"));
        ServletOutputStream out = response.getOutputStream();
        ByteArrayInputStream inputStream = new ByteArrayInputStream(Base64.getDecoder().decode(value.get("image")));
        ImageIO.write(ImageIO.read(inputStream), "jpg", out);
        try {
            out.flush();
        } finally {
            out.close();
        }
    }

    @RequestMapping(value = "/checkVerifier", method = RequestMethod.GET)
    @ApiOperation(value = "验证码检测", notes = "检测验证码")
    public String checkVerifier(HttpServletRequest request, @RequestParam("verifyCode") String verifyCode) {
        String captchaId = (String) request.getSession().getAttribute(VERIFIER_CODE);
        LOGGER.info("验证码：{}，输入码：{}", captchaId, verifyCode);
        JSONObject retString = new JSONObject();
        if (!captchaId.equalsIgnoreCase(verifyCode)) {
            retString.put("code", -100);
            retString.put("message", "错误的验证码");
        } else {
            retString.put("code", 0);
            retString.put("message", "验证成功");
        }
        return retString.toJSONString();
    }

}
