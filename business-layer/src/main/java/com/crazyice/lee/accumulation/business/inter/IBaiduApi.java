package com.crazyice.lee.accumulation.business.inter;

import com.alibaba.fastjson.JSONObject;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "baidu-api")
public interface IBaiduApi {
    @GetMapping(value = "/oauth/2.0/token")
    JSONObject getAccessToken(@RequestParam("grant_type") String grant_type, @RequestParam("client_id") String client_id, @RequestParam("client_secret") String client_secret);

    @PostMapping(value = "/rest/2.0/image-classify/v2/advanced_general", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    JSONObject getObjectInfo(@RequestParam("access_token") String access_token, @RequestBody String image);

    @PostMapping(value = "/rest/2.0/image-classify/v1/car", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    JSONObject getCarInfo(@RequestParam("access_token") String access_token, @RequestBody String image);

    @PostMapping(value = "/rest/2.0/ocr/v1/idcard", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    JSONObject getIDCardInfo(@RequestParam("access_token") String access_token, @RequestParam("id_card_side") String side, @RequestBody String image);

    @PostMapping(value = "/rest/2.0/ocr/v1/accurate_basic", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    JSONObject getImageTextInfo(@RequestParam("access_token") String access_token, @RequestBody String image);

    @PostMapping(value = "/rest/2.0/face/v3/detect?image_type=BASE64&max_face_num=10", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    JSONObject getFaceInfo(@RequestParam("access_token") String access_token, @RequestBody String image);

}
