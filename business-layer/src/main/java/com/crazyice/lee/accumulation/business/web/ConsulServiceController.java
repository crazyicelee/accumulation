package com.crazyice.lee.accumulation.business.web;

import com.alibaba.fastjson.JSONObject;
import com.ecwid.consul.v1.ConsulClient;
import com.ecwid.consul.v1.Response;
import com.ecwid.consul.v1.agent.model.Member;
import com.ecwid.consul.v1.agent.model.NewService;
import com.ecwid.consul.v1.agent.model.Service;
import com.ecwid.consul.v1.health.model.Check;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;


@RestController
public class ConsulServiceController {
    private static Logger log = LoggerFactory.getLogger(ConsulServiceController.class);
    @Autowired
    private ConsulClient consulClient;

    @RequestMapping(value = "/regservice", method = RequestMethod.POST)
    @ApiOperation(value = "注册第三方API成为服务", notes = "注册第三方API")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "id", value = "微服务ID", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "query", name = "tag", value = "商用逗号分隔的标签", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "query", name = "name", value = "微服务唯一名称", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "query", name = "port", value = "微服务端口号", required = true, dataType = "int"),
            @ApiImplicitParam(paramType = "query", name = "domain", value = "API域名或者IP", required = true, dataType = "String"),
            @ApiImplicitParam(paramType = "query", name = "healthUrl", value = "API健康监测地址", dataType = "String")
    })
    public Boolean registerService(@RequestParam(value = "id") String serviceID, @RequestParam(value = "tag") String tag, @RequestParam(value = "name") String serviceName, @RequestParam(value = "port") int port, @RequestParam(value = "domain") String domain, @RequestParam(value = "healthUrl", required = false) String healthUrl) {
        //判断serviceName或者serviceID是否已经存在
        List<Member> members = consulClient.getAgentMembers().getValue();
        for (int i = 0; i < members.size(); i++) {
            String role = members.get(i).getTags().get("role");
            //判断是否为client
            if (role.equals("node")) {
                String address = members.get(i).getAddress();
                //将IP地址传给ConsulClient的构造方法，获取对象
                ConsulClient clearClient = new ConsulClient(address);
                //根据clearClient，获取当前IP下所有的服务 使用迭代方式 获取map对象的值
                Iterator<Map.Entry<String, Service>> it = clearClient.getAgentServices().getValue().entrySet().iterator();
                while (it.hasNext()) {
                    //迭代数据
                    Map.Entry<String, Service> serviceMap = it.next();
                    //获得Service对象
                    Service service = serviceMap.getValue();
                    if (serviceID.equalsIgnoreCase(service.getId()) || serviceName.equalsIgnoreCase(service.getService())) {
                        return false;
                    }
                }
            }
        }
        //组装服务信息
        List<String> tags = Arrays.asList(tag.split(","));
        NewService newService = new NewService();
        newService.setId(serviceID);
        newService.setTags(tags);
        newService.setName(serviceName);
        newService.setPort(port);
        newService.setAddress(domain);
        //组装健康检查信息
        if (healthUrl != null) {
            NewService.Check serviceCheck = new NewService.Check();
            serviceCheck.setHttp(healthUrl);
            serviceCheck.setInterval("30s");
            newService.setCheck(serviceCheck);
        }
        consulClient.agentServiceRegister(newService);
        return true;
    }

    @RequestMapping(value = "/clearservice", method = RequestMethod.GET)
    @ApiOperation(value = "注销指定的微服务", notes = "注销微服务")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "name", value = "微服务名称", required = true, dataType = "String")
    })
    public Boolean clearService(@RequestParam(value = "name") String serviceName) {
        List<Member> members = consulClient.getAgentMembers().getValue();
        for (int i = 0; i < members.size(); i++) {
            String role = members.get(i).getTags().get("role");
            //判断是否为client
            if (role.equals("node")) {
                String address = members.get(i).getAddress();
                //将IP地址传给ConsulClient的构造方法，获取对象
                ConsulClient clearClient = new ConsulClient(address);
                //根据clearClient，获取当前IP下所有的服务 使用迭代方式 获取map对象的值
                Iterator<Map.Entry<String, Service>> it = clearClient.getAgentServices().getValue().entrySet().iterator();
                while (it.hasNext()) {
                    //迭代数据
                    Map.Entry<String, Service> serviceMap = it.next();
                    //获得Service对象
                    Service service = serviceMap.getValue();
                    if (serviceName.equalsIgnoreCase(service.getService())) {
                        clearClient.agentServiceDeregister(service.getId());
                        log.info("清除了{}:{}", service.getService(), service.getId());
                    }
                }
            }
        }
        return true;
    }

    @RequestMapping(value = "/clear", method = RequestMethod.GET)
    @ApiOperation(value = "清除无效的微服务", notes = "清理掉consul上无效的微服务")
    public JSONObject allService() {
        JSONObject returnVal = new JSONObject();

        log.info("***********************consul上无效服务清理开始*******************************************");
        //获取所有的members的信息
        List<Member> members = consulClient.getAgentMembers().getValue();
        for (int i = 0; i < members.size(); i++) {
            //获取每个member的IP地址
            String address = members.get(i).getAddress();
            log.info("member的IP地址为:{}", address);
            //根据role变量获取每个member的角色  role：consul---代表服务端   role：node---代表客户端
            String role = members.get(i).getTags().get("role");
            log.info("{}机器的role为：{}", address, role);
            //判断是否为client
            if (role.equals("node")) {
                //将IP地址传给ConsulClient的构造方法，获取对象
                ConsulClient clearClient = new ConsulClient(address);
                //根据clearClient，获取当前IP下所有的服务 使用迭代方式 获取map对象的值
                Iterator<Map.Entry<String, Service>> it = clearClient.getAgentServices().getValue().entrySet().iterator();
                while (it.hasNext()) {
                    //迭代数据
                    Map.Entry<String, Service> serviceMap = it.next();
                    //获得Service对象
                    Service service = serviceMap.getValue();
                    //获取服务名称
                    String serviceName = service.getService();
                    //获取服务ID
                    String serviceId = service.getId();
                    //根据服务名称获取服务的健康检查信息
                    Response<List<Check>> checkList = consulClient.getHealthChecksForService(serviceName, null);
                    checkList.getValue().stream().map(e -> {
                        //获取健康状态值  PASSING：正常  WARNING  CRITICAL  UNKNOWN：不正常
                        Check.CheckStatus checkStatus = e.getStatus();
                        log.info("服务名称 :{}服务ID:{}的健康状态值：{}", serviceName, serviceId, checkStatus);
                        returnVal.put(serviceId, checkStatus.toString());
                        if (checkStatus != Check.CheckStatus.PASSING) {
                            log.info("服务名称 :{}为无效服务，准备清理...................", address, serviceName);
                            clearClient.agentServiceDeregister(serviceId);
                        }
                        return null;
                    });
                }
            }
        }
        return returnVal;
    }
}