package com.crazyice.lee.accumulation.business.inter;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@FeignClient(name = "accumulation-data-dictionary-layer")
public interface IDictionary {
    @RequestMapping(method = RequestMethod.GET, value = "/getDictionary/{name}")
    Map getDataDictionary(@PathVariable("name") String name, @RequestParam(value = "filter", required = false) String filter);
}
