package com.crazyice.lee.accumulation.business.web;

import com.alibaba.fastjson.JSONObject;
import com.crazyice.lee.accumulation.business.inter.IOrgOauth;
import com.crazyice.lee.accumulation.business.inter.IPersonalCockpit;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrgPlatformController {
    private static final Logger LOGGER = LoggerFactory.getLogger(OrgPlatformController.class);

    @Autowired
    private IOrgOauth iOrgOauth;

    @Autowired
    private IPersonalCockpit personalCockpit;

    @GetMapping(value = "/getUserInfo")
    @ApiOperation(value = "获取登录用户信息", notes = "从组织平台API获取登录用户信息")
    public JSONObject getUserInfo(@RequestParam(value = "code") String code) {
        JSONObject accessToken = iOrgOauth.getAccessToken(code);
        if (accessToken.getInteger("code") == 0) {
            return iOrgOauth.getUserInfo(accessToken.getJSONObject("data").getString("accessToken"));
        } else {
            JSONObject result = new JSONObject();
            result.put("code", accessToken.getInteger("code"));
            result.put("message", accessToken.getString("message"));
            return result;
        }
    }

    @GetMapping(value = "/getCockpit")
    @ApiOperation(value = "获取登录用户驾驶舱数据", notes = "按照登录用户获取个人工作台的驾驶舱")
    public JSONObject getCockpit(@RequestParam(value = "code") String code) {
        JSONObject result = new JSONObject();
        JSONObject accessToken = iOrgOauth.getAccessToken(code);
        if (accessToken.getInteger("code") == 0) {
            JSONObject userInfo = iOrgOauth.getUserInfo(accessToken.getJSONObject("data").getString("accessToken"));
            JSONObject data = userInfo.getJSONObject("data");
            result.put("code", 0);
            result.put("message", "成功");
            result.put("data", personalCockpit.getCockpit(data.getString("mobile")));
            return result;
        } else {
            result.put("code", accessToken.getInteger("code"));
            result.put("message", accessToken.getString("message"));
            return result;
        }
    }
}
