package com.crazyice.lee.accumulation.business.inter;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "aliyun-moji-api")
public interface IAliyunMojiApi {
    @PostMapping(value = "/whapi/json/alicityweather/briefforecast3days", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    String getBriefForecast3Days(@RequestParam("cityId") String cityId, @RequestHeader("Authorization") String header);
}