package com.crazyice.lee.accumulation.business.inter;

import com.alibaba.fastjson.JSONObject;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "aliyun-taiyue-api")
public interface IAliyunTaiYueApi {
    @PostMapping(value = "hanxuanliaotian", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    JSONObject hanxuan(@RequestParam("in") String cityId, @RequestHeader("Authorization") String header);
}