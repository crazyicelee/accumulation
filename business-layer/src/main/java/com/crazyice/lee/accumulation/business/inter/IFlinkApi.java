package com.crazyice.lee.accumulation.business.inter;

import com.alibaba.fastjson.JSONObject;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

@FeignClient(name = "syswinrpc-flink")
public interface IFlinkApi {
    @RequestMapping(method = RequestMethod.GET, value = "/overview")
    JSONObject overview();

    @RequestMapping(method = RequestMethod.GET, value = "/jars")
    JSONObject jars();

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE, value = "/jars/upload")
    JSONObject jarUpload(@RequestPart(value = "jarfile") MultipartFile jarfile);
}
