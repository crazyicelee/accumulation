package com.crazyice.lee.accumulation.business.inter;

import com.alibaba.fastjson.JSONObject;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "baidu-vop-api")
public interface IBaiduVopApi {
    @PostMapping(value = "/pro_api?channel=1&dev_pid=80001", consumes = "audio/pcm;rate=16000")
    JSONObject voiceToText(@RequestParam("token") String token, @RequestParam("cuid") String cuid, @RequestBody byte[] speech);

}
