package com.crazyice.lee.accumulation.business.web;

import com.alibaba.fastjson.JSONObject;
import com.crazyice.lee.accumulation.business.conf.AliyunDataConf;
import com.crazyice.lee.accumulation.business.conf.BaiduAIConf;
import com.crazyice.lee.accumulation.business.conf.JuheDataConf;
import com.crazyice.lee.accumulation.business.inter.*;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.UUID;

@RestController
@CrossOrigin
@Slf4j
public class ThirdApiController {

    @Value("${imageUrl}")
    private String imageUrl;

    @Autowired
    private JuheDataConf juheDataConf;

    @Autowired
    private BaiduAIConf baiduAIConf;

    @Autowired
    private AliyunDataConf aliyunDataConf;

    @Autowired
    private IJuheApi juheApi;

    @Autowired
    private IBaiduApi baiduApi;

    @Autowired
    private IBaiduVopApi baiduVopApi;

    @Autowired
    private IDistributionFile distributionFile;

    @Autowired
    private IAliyunMojiApi aliyunMojiApi;

    @Autowired
    private IAliyunTaiYueApi aliyunTaiYueApi;

    @Autowired
    private IAliyunHuaChenApi aliyunHuaChenApi;

    @Autowired
    private IFlinkApi flinkApi;

    @Autowired
    private IAzkabanApi azkabanApi;

    @RequestMapping(value = "/juhe/movie", method = RequestMethod.GET)
    @ApiOperation(value = "聚合数据-影视信息", notes = "获取指定影视片名的信息")
    public JSONObject getMovieInfo(@RequestParam(value = "title", defaultValue = "白日焰火") String title) {
        return juheApi.getMovieInfo(title, juheDataConf.getMovieKey());
    }

    @PostMapping(value = "/juhe/IDCard")
    @ApiOperation(value = "聚合数据-证件识别", notes = "将证件照图片上传到服务器并进行识别")
    public JSONObject cardPicCertificate(@RequestPart("pic") MultipartFile pic) {
        return juheApi.getCertificateInfo("2", pic, juheDataConf.getIdCardKey());
    }

    /**
     * 百度API接口调用：根据key获取accesstoken
     *
     * @param key
     * @param secret
     * @param file
     * @return
     * @throws IOException
     */
    private JSONObject baiduAIImageBase64(String key, String secret, MultipartFile file) throws IOException {
        JSONObject result = new JSONObject();
        JSONObject token = baiduApi.getAccessToken("client_credentials", key, secret);
        String access_token = token.getString("access_token");
        String urlBase64Image = URLEncoder.encode(Base64.getEncoder().encodeToString(file.getBytes()), "UTF-8");
        if (!access_token.isEmpty() && !urlBase64Image.isEmpty()) {
            result.put("token", access_token);
            result.put("base64", urlBase64Image);
        }
        return result;
    }

    @PostMapping(value = "/baiduAI/Car")
    @ApiOperation(value = "百度图像识别-汽车品牌", notes = "使用百度AI识别图像")
    public JSONObject getBaiduAIInfo(@RequestPart("file") MultipartFile file) throws IOException {
        JSONObject result = baiduAIImageBase64(baiduAIConf.getImage().get("clientId"), baiduAIConf.getImage().get("clientSecret"), file);
        if (!result.isEmpty()) {
            return baiduApi.getCarInfo(result.getString("token"), "image=" + result.getString("base64"));
        }
        return null;
    }

    @PostMapping(value = "/baiduAI/Object")
    @ApiOperation(value = "百度图像识别-物体检测", notes = "使用百度AI识别图像")
    public JSONObject getBaiduAIObjectInfo(@RequestPart("file") MultipartFile file) throws IOException {
        JSONObject result = baiduAIImageBase64(baiduAIConf.getImage().get("clientId"), baiduAIConf.getImage().get("clientSecret"), file);
        if (!result.isEmpty()) {
            return baiduApi.getObjectInfo(result.getString("token"), "image=" + result.getString("base64"));
        }
        return null;
    }

    @PostMapping(value = "/baiduAI/Text")
    @ApiOperation(value = "百度OCR-文字", notes = "使用百度OCR识别图像")
    public JSONObject getBaiduAITextInfo(@RequestPart("file") MultipartFile file) throws IOException {
        JSONObject result = baiduAIImageBase64(baiduAIConf.getOcr().get("clientId"), baiduAIConf.getOcr().get("clientSecret"), file);
        if (!result.isEmpty()) {
            return baiduApi.getImageTextInfo(result.getString("token"), "image=" + result.getString("base64"));
        }
        return null;
    }

    @PostMapping(value = "/baiduAI/IDCard")
    @ApiOperation(value = "百度OCR-身份证", notes = "使用百度OCR识别图像")
    public JSONObject getBaiduAIIDCardInfo(@RequestPart("file") MultipartFile file) throws IOException {
        JSONObject result = baiduAIImageBase64(baiduAIConf.getOcr().get("clientId"), baiduAIConf.getOcr().get("clientSecret"), file);
        if (!result.isEmpty()) {
            return baiduApi.getIDCardInfo(result.getString("token"), "front", "image=" + result.getString("base64"));
        }
        return null;
    }

    @PostMapping(value = "/baiduAI/Face")
    @ApiOperation(value = "百度人脸识别-裁剪图片中包含的人脸", notes = "使用百度人脸识别图像")
    public JSONObject getBaiduAIFaceInfo(@RequestPart("file") MultipartFile file) throws IOException {
        BufferedImage bufferedImage = ImageIO.read(file.getInputStream());

        JSONObject result = baiduAIImageBase64(baiduAIConf.getFace().get("clientId"), baiduAIConf.getFace().get("clientSecret"), file);
        if (!result.isEmpty()) {
            List<String> faceUrls = new ArrayList<>();

            JSONObject faceList = baiduApi.getFaceInfo(result.getString("token"), "image=" + result.getString("base64"));
            faceList.getJSONObject("result").getJSONArray("face_list").stream().forEach(e -> {
                JSONObject face = (JSONObject) e;
                int left = face.getJSONObject("location").getIntValue("left");
                int top = face.getJSONObject("location").getIntValue("top");
                int width = face.getJSONObject("location").getIntValue("width");
                int height = face.getJSONObject("location").getIntValue("height");
                //裁剪人脸图像并保存到分布式文件系统
                BufferedImage subBufferedImage = bufferedImage.getSubimage(left, top, width, height);
                try {
                    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
                    ImageIO.write(subBufferedImage, "JPG", outputStream);
                    outputStream.flush();
                    outputStream.close();
                    InputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
                    MultipartFile subFaceFile = new MockMultipartFile("file", inputStream);
                    inputStream.close();
                    faceUrls.add(imageUrl + distributionFile.save(subFaceFile));
                } catch (IOException ex) {
                    log.error("{}", ex.getLocalizedMessage());
                }
            });
            JSONObject returnValue = new JSONObject();
            returnValue.put("status", 0);
            returnValue.put("faceUrls", faceUrls);
            return returnValue;
        }
        return null;
    }

    @PostMapping(value = "/baiduAI/Vop")
    @ApiOperation(value = "百度语音-语音转文字", notes = "使用百度语音转换")
    public JSONObject voiceToText(@RequestPart("file") MultipartFile file) throws IOException {
        JSONObject result = baiduAIImageBase64(baiduAIConf.getVoice().get("clientId"), baiduAIConf.getVoice().get("clientSecret"), file);
        if (!result.isEmpty()) {
            return baiduVopApi.voiceToText(result.getString("token"), UUID.randomUUID().toString(), file.getBytes());
        }
        return null;
    }

    @PostMapping(value = "/aliyun/weather")
    @ApiOperation(value = "阿里云API-墨迹天气", notes = "使用阿里云数据接口")
    public JSONObject getAliyunWeatherInfo(@RequestParam("cityId") String cityId) {
        return new JSONObject().parseObject(aliyunMojiApi.getBriefForecast3Days(cityId, "APPCODE " + aliyunDataConf.getMojitianqiKey()));
    }

    @PostMapping(value = "/aliyun/hanxuan")
    @ApiOperation(value = "阿里云API-机器人聊天", notes = "使用阿里云数据接口")
    public JSONObject getAliyunHanXuan(@RequestParam("in") String in) {
        return aliyunTaiYueApi.hanxuan(in, "APPCODE " + aliyunDataConf.getMojitianqiKey());
    }

    @PostMapping(value = "/aliyun/tags")
    @ApiOperation(value = "阿里云API-文章标签", notes = "使用阿里云数据接口")
    public JSONObject getAliyunTopicTags(@RequestParam("title") String title, @RequestParam("content") String content) {
        return aliyunHuaChenApi.getTopicTag(title, content, "APPCODE " + aliyunDataConf.getMojitianqiKey());
    }

    @GetMapping(value = "/flink/overview")
    @ApiOperation(value = "FlinkAPI-任务状态全览", notes = "使用Flink接口")
    public JSONObject getFlinkOverview() {
        return flinkApi.overview();
    }

    @GetMapping(value = "/flink/jars")
    @ApiOperation(value = "FlinkAPI-上传的jar列表", notes = "使用Flink接口")
    public JSONObject getFlinkJars() {
        return flinkApi.jars();
    }

    @PostMapping(value = "/flink/jars/upload")
    @ApiOperation(value = "FlinkAPI-上传的jar包", notes = "使用Flink接口")
    public JSONObject postFlinkJar(@RequestPart("jarfile") MultipartFile jarfile) {
        return flinkApi.jarUpload(jarfile);
    }

    @PostMapping(value = "/azkaban/login")
    @ApiOperation(value = "AzkabanAPI-上传的jar包", notes = "使用Azkaban接口")
    public JSONObject login(@RequestParam("username") String username, @RequestParam("password") String password) {
        JSONObject data = new JSONObject();
        data.put("action", "login");
        data.put("username", username);
        data.put("password", password);
        return azkabanApi.login(data);
    }

}
