package com.crazyice.lee.accumulation.business.inter;

import com.alibaba.fastjson.JSONObject;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "aliyun-huachen-api")
public interface IAliyunHuaChenApi {
    @PostMapping(value = "/nlp/keyword", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    JSONObject getTopicTag(@RequestParam("title") String title, @RequestParam("content") String content, @RequestHeader("Authorization") String header);
}