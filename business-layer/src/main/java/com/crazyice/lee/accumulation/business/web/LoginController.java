package com.crazyice.lee.accumulation.business.web;

import com.alibaba.fastjson.JSONObject;
import com.crazyice.lee.accumulation.business.data.RegisterVO;
import com.crazyice.lee.accumulation.business.utils.JwtUtil;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

@RestController
@CrossOrigin
public class LoginController {
    private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);

    @Value("${jwtSecret}")
    private String jwtSecret;
    @Value("${tokenExpireTime}")
    private Long tokenExpireTime;

    @GetMapping(value = "/index")
    @ApiOperation(value = "成功登录", notes = "提供API网关验证后获取数据")
    public JSONObject index(@RequestParam(value = "id") String id, @RequestParam(value = "username") String username) {
        JSONObject result = new JSONObject();
        try {
            RegisterVO user = new RegisterVO();
            user.setPassword(jwtSecret);
            result.put("code", 1000);
            result.put("id", id);
            result.put("username", username);
        } catch (Exception e) {
            LOGGER.error("{}", e.getLocalizedMessage());
            result.put("code", -1000);
            result.put("message", e.getLocalizedMessage());
        }
        return result;
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ApiOperation(value = "登录", notes = "用户名、密码登录")
    public JSONObject login(HttpServletResponse response, RegisterVO user) {
        user.setId(UUID.randomUUID().toString());
        user.setPassword(jwtSecret);
        Cookie cookie = new Cookie("accessToken", JwtUtil.createJWT(tokenExpireTime, user));
        response.addCookie(cookie);
        JSONObject result = new JSONObject();
        result.put("code", 1000);
        result.put("message", "登录成功");
        return result;
    }

}
