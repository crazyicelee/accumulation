package com.crazyice.lee.accumulation.business.conf;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "aliyun.data")
@Data
public class AliyunDataConf {
    private String mojitianqiKey;
    private String taiyueHanXuanKey;
    private String huachenTopicTagKey;
}
