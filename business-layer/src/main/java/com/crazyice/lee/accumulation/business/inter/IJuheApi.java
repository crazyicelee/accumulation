package com.crazyice.lee.accumulation.business.inter;

import com.alibaba.fastjson.JSONObject;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@FeignClient(name = "juhe-api")
public interface IJuheApi {
    @RequestMapping(value = "/movie/index", method = RequestMethod.GET)
    JSONObject getMovieInfo(@RequestParam("title") String title, @RequestParam("key") String key);

    @PostMapping(value = "/certificates/query.php", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    JSONObject getCertificateInfo(@RequestParam("cardType") String cardType, @RequestPart("pic") MultipartFile pic, @RequestParam("key") String key);
}
