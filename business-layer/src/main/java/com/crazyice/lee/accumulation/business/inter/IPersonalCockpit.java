package com.crazyice.lee.accumulation.business.inter;

import com.alibaba.fastjson.JSONObject;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "accumulation-personal-cockpit-layer")
public interface IPersonalCockpit {
    @RequestMapping(value = "/getCockpit/{userId}", method = RequestMethod.GET)
    JSONObject getCockpit(@PathVariable("userId") String userId);
}
