package com.crazyice.lee.accumulation.business.inter;

import com.alibaba.fastjson.JSONObject;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "org-oauth")
@RequestMapping("/openplatform/oauth")
public interface IOrgOauth {
    @RequestMapping(value = "/getAccessToken", method = RequestMethod.GET)
    JSONObject getAccessToken(@RequestParam("code") String code);

    @RequestMapping(value = "/open/getUserInfo", method = RequestMethod.GET)
    JSONObject getUserInfo(@RequestHeader("X-Access-Token") String token);
}
