package com.crazyice.lee.accumulation.thread.safety;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Callable;

@Slf4j
class SubTask implements Callable<Integer> {
    private Integer taskId;

    public SubTask(Integer taskId) {
        this.taskId = taskId;
    }

    @Override
    public Integer call() throws Exception {
        log.info("子线程{}在进行计算", taskId);
        Thread.sleep(1000);
        int sum = 0;
        for (int i = 0; i < 100; i++)
            sum++;
        return sum;
    }
}