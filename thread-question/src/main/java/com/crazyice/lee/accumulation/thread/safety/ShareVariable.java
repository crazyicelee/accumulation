package com.crazyice.lee.accumulation.thread.safety;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@Slf4j
@Data
@Component
public class ShareVariable {
    private volatile int x = 0;
    private AtomicInteger y = new AtomicInteger(0);
    private ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    private Lock readLock = lock.readLock();
    private Lock writeLock = lock.writeLock();

    //非线程安全操作
    private void nosafeCount() {
        x++;
    }

    //线程安全操作
    private synchronized void safeCount() {
        x++;
    }

    //atomic线程安全操作
    private void atomicCount() {
        y.getAndIncrement();
    }

    //lock线程安全操作
    private void lockSafeCount() {
        writeLock.lock();
        try {
            x++;
        } finally {
            writeLock.unlock();
        }
    }

    public void runTest() {
        new Thread(() -> {
            for (int i = 0; i < 1_000_000; i++) {
                lockSafeCount();
            }
            log.info("final count from 1:{} ", x);
        }).start();

        new Thread(() -> {
            for (int i = 0; i < 1_000_000; i++) {
                lockSafeCount();
            }
            log.info("final count from 2:{} ", x);
        }).start();
    }

    //future线程模式演示
    public void futureTest() {
        //启动子线程
        List<FutureTask<Integer>> futureTaskList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            SubTask task = new SubTask(i);
            futureTaskList.add(new FutureTask<>(task));
            new Thread(futureTaskList.get(i)).start();
        }

        //获取子线程执行结果
        try {
            for (int i = 0; i < 10; i++) {
                log.info("task{}运行结果{}", i, futureTaskList.get(i).get());
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        log.info("所有任务执行完毕");
    }

    //CompleteFuture模式演示
    public void completableFutureDemo() {
        CompletableFuture<String> ruleFutureA = CompletableFuture.supplyAsync(() -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                log.error(e.getLocalizedMessage());
            }
            return "hello A";
        });
        CompletableFuture<String> ruleFutureB = CompletableFuture.supplyAsync(() -> {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                log.error(e.getLocalizedMessage());
            }
            return "hello B";
        });

        int isAll = 1;
        switch (isAll) {
            //两个线程任何一个先执行完
            case 0:
                CompletableFuture<Object> anyOf = CompletableFuture.anyOf(ruleFutureA, ruleFutureB);
                anyOf.join();
                break;
            //两个线程都执行完
            case 1:
                CompletableFuture<Void> allOf = CompletableFuture.allOf(ruleFutureA, ruleFutureB);
                allOf.join();
                break;
        }

        //获取线程执行结果
        try {
            String returnA = ruleFutureA.get();
            log.info("{}:{}", System.currentTimeMillis(), returnA);
            String returnB = ruleFutureB.get();
            log.info("{}:{}", System.currentTimeMillis(), returnB);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }
}
