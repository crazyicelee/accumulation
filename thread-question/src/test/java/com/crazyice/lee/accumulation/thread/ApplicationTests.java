package com.crazyice.lee.accumulation.thread;

import com.crazyice.lee.accumulation.thread.safety.ShareVariable;
import lombok.extern.slf4j.Slf4j;
import org.jasypt.encryption.StringEncryptor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ApplicationTests {

    @Autowired
    private ShareVariable shareVariable;

    @Autowired
    private StringEncryptor stringEncryptor;

    @Value("${myparameter}")
    private String myparameter;

    @Test
    public void contextLoads() {
    }

    //@Test
    public void nosafeTest() {
        shareVariable.runTest();
        while (true) {
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    //@Test
    public void futureTaskTest() {
        shareVariable.futureTest();
    }

    //@Test
    public void readMyparameterTest() {
        log.info("解密后的参数值={}", myparameter);
    }

    //@Test
    public void encryptTest() {
        String dest = stringEncryptor.encrypt("我是一个自豪的中国人！");
        log.info("加密后的参数值={}", dest);
    }

    @Test
    public void CompletableFutureTest() {
        shareVariable.completableFutureDemo();
    }

}

