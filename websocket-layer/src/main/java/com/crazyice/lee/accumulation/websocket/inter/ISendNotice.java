package com.crazyice.lee.accumulation.websocket.inter;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "accumulation-message-layer")
public interface ISendNotice {
    @RequestMapping(method = RequestMethod.GET, value = "/send/a.oa-hr")
    String send(@RequestParam(value = "m") String message);
}
