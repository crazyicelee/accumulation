package com.crazyice.lee.accumulation.websocket.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {
    private String uId;
    private String uName;
    private Boolean isOnline;
}
