package com.crazyice.lee.accumulation.websocket.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Notice {
    private String myTemail;
    private String bizNo;
    private String subCatalog;
    private String message;
    private String buttonText;
    private String toonUrl;
}
