package com.crazyice.lee.accumulation.websocket.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.crazyice.lee.accumulation.websocket.conf.ThirdParam;
import com.crazyice.lee.accumulation.websocket.data.Notice;
import com.crazyice.lee.accumulation.websocket.data.User;
import com.crazyice.lee.accumulation.websocket.inter.ISendNotice;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@ServerEndpoint(value = "/im/{uId}/{uName}")
@Component
public class WebSocketServer {
    private static final Logger LOGGER = LoggerFactory.getLogger(WebSocketServer.class);
    //此处是解决无法注入的关键
    private static ApplicationContext applicationContext;
    //注入的service
    private static ISendNotice iSendNotice;
    //注入动态配置
    private static ThirdParam thirdParam;
    //注入本地缓存service
    private static CacheServer cacheServer;

    //concurrent包的线程安全Set，用来存放每个客户端对应的MyWebSocket对象。
    private static ConcurrentHashMap<String, WebSocketServer> websocketList = new ConcurrentHashMap<>();
    //登录用户列表，存储全部用户信息，包括从系统启动初所有数据
    private static ConcurrentHashMap<String, User> loginUsers = new ConcurrentHashMap<>();

    //与某个客户端的连接会话，需要通过它来给客户端发送数据
    private Session session;
    //登录用户
    private User user;

    //外部bean注入进来
    public static void setApplicationContext(ApplicationContext applicationContext) {
        WebSocketServer.applicationContext = applicationContext;
        WebSocketServer.iSendNotice = applicationContext.getBean(ISendNotice.class);
        WebSocketServer.thirdParam = applicationContext.getBean(ThirdParam.class);
        WebSocketServer.cacheServer = applicationContext.getBean(CacheServer.class);
        //从缓存初始化登录用户
        JSONArray cacheUsers = cacheServer.readCacheUsers();
        LOGGER.info("从缓存中读取的已经登录用户：{}", cacheUsers);
        cacheUsers.stream().forEach(e -> {
            User user = (User) e;
            WebSocketServer.loginUsers.put(user.getUId(), user);
        });
    }

    /**
     * 连接建立成功调用的方法
     */
    @OnOpen
    public void onOpen(Session session, @PathParam("uId") String uId, @PathParam("uName") String uName) {
        this.session = session;
        this.user = new User(uId, uName, true);
        websocketList.put(this.user.getUId(), this);
        loginUsers.put(this.user.getUId(), this.user);
        updateOnlineUsers();
        LOGGER.info("有新用户登录，开始监听{},{}", user, thirdParam);
    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose() {
        this.user.setIsOnline(false);
        websocketList.get(this.user.getUId()).user.setIsOnline(false);
        loginUsers.get(this.user.getUId()).setIsOnline(false);
        updateOnlineUsers();
        LOGGER.info("有一连接关闭！");
    }

    /**
     * 收到客户端消息后调用的方法
     *
     * @param message 客户端发送过来的消息
     */
    @OnMessage
    public void onMessage(String message, Session session) {
        LOGGER.info("收到来{}的信息{}", user, message);
        if (StringUtils.isNotBlank(message)) {
            //解析发送的报文
            JSONObject object = JSONObject.parseObject(message);
            int type = object.getInteger("Type");
            switch (type) {
                case 1://群发全部用户
                    break;
                case 2://传送给对应用户的websocket
                    User from = object.getJSONObject("Data").getJSONObject("from").toJavaObject(User.class);
                    User to = object.getJSONObject("Data").getJSONObject("to").toJavaObject(User.class);
                    String content = object.getJSONObject("Data").getString("content");
                    //接收用户在线，则发送聊天消息，否则发送通知邀请上线
                    if (to.getIsOnline()) {
                        WebSocketServer webSocketServer = websocketList.get(to.getUId());
                        if (webSocketServer != null) {
                            object.put("Type", 3);
                            try {
                                webSocketServer.sendMessage(object.toJSONString());
                            } catch (IOException e) {
                                LOGGER.error("发送消息异常");
                                e.printStackTrace();
                            }
                        }
                    } else {
                        JSONArray notices = new JSONArray();
                        Notice notice = new Notice();
                        notice.setBizNo(UUID.randomUUID().toString());
                        notice.setMyTemail(to.getUId());
                        notice.setSubCatalog("邀请在线沟通");
                        notice.setMessage(from.getUId() + "给你发送消息：" + content);
                        notice.setButtonText(thirdParam.getNoticeButtonText());
                        notice.setToonUrl(thirdParam.getNoticeButtonUrl());
                        notices.add(notice);
                        String returnString = iSendNotice.send(notices.toJSONString());
                        LOGGER.info("发送秘邮通知{}，结果：{}", notice, returnString);
                    }
                    break;
            }
        }
    }

    /**
     * @param session
     * @param error
     */
    @OnError
    public void onError(Session session, Throwable error) {
        error.printStackTrace();
    }

    /**
     * 更新在线用户列表并同步给客户端
     */
    private void updateOnlineUsers() {
        JSONArray userList = new JSONArray();
        userList.addAll(loginUsers.values());
        if (!userList.isEmpty()) {
            //更新缓存
            cacheServer.writeCacheUsers(userList);
            //给客户端推送消息，更新用户列表
            try {
                JSONObject message = new JSONObject();
                message.put("Type", 1);
                message.put("Data", userList);
                sendInfo(message.toJSONString());
            } catch (IOException e) {
                LOGGER.error("websocket IO异常");
            }
        }
        LOGGER.info("用户列表{}", loginUsers);
    }

    /**
     * 实现服务器主动推送
     */
    private void sendMessage(String message) throws IOException {
        this.session.getBasicRemote().sendText(message);
    }

    /**
     * 群发自定义消息
     */
    private void sendInfo(String message) throws IOException {
        websocketList.values().stream().forEach(item -> {
            if (item.user.getIsOnline()) {
                try {
                    item.sendMessage(message);
                    LOGGER.info("发送消息：{}给：{}", message, item.user.getUId());
                } catch (Exception e) {
                    LOGGER.error("{}", e.getLocalizedMessage());
                }
            }
        });
    }

    @Override
    public String toString() {
        return "{" + user + "}";
    }
}
