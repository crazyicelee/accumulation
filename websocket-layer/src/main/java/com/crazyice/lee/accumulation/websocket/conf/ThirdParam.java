package com.crazyice.lee.accumulation.websocket.conf;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "notice")
@Data
public class ThirdParam {
    private String noticeButtonText;
    private String noticeButtonUrl;
}
