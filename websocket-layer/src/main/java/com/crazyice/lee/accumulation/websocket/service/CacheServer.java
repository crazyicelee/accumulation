package com.crazyice.lee.accumulation.websocket.service;

import com.alibaba.fastjson.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

@Component
@CacheConfig(cacheNames = "userdata")
public class CacheServer {
    private static final Logger LOGGER = LoggerFactory.getLogger(CacheServer.class);
    //注意：key字符串要用单引号括住
    private static final String CACHE_KEY = "'websocket'";

    /**
     * 将已经登录的用户写入缓存
     *
     * @param userList
     * @return
     */
    @CachePut(key = CACHE_KEY)
    public JSONArray writeCacheUsers(JSONArray userList) {
        return userList;
    }

    /**
     * 从缓存读取已经登录的用户
     */
    @Cacheable(key = CACHE_KEY)
    public JSONArray readCacheUsers() {
        JSONArray userList = new JSONArray();
        return userList;
    }

}
