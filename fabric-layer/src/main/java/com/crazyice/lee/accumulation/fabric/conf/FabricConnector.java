package com.crazyice.lee.accumulation.fabric.conf;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "farbic.connection")
@Data
public class FabricConnector {
}
