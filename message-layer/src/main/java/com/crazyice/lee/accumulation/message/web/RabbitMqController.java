package com.crazyice.lee.accumulation.message.web;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;

@RestController
public class RabbitMqController {
    private static final Logger LOGGER = LoggerFactory.getLogger(RabbitMqController.class);

    @Autowired
    private AmqpTemplate template;

    @Value(value = "${exchangeName}")
    private String exchangeName;

    @Value(value = "${queueName}")
    private String queueName;

    @Value(value = "${queueTopic1}")
    private String queueTopic1;

    @Value(value = "${queueTopic2}")
    private String queueTopic2;

    @RequestMapping(value = "/send", method = RequestMethod.GET)
    @ApiOperation(value = "发送消息", notes = "向Rabbit消息队列发送一条消息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "m", value = "消息内容", required = true, dataType = "String")
    })
    public String testRabbit(@RequestParam("m") String message) {
        try {
            template.convertAndSend(queueName, message);
            template.convertAndSend(exchangeName, queueTopic1, message);
        } catch (Exception e) {
            LOGGER.error("发送消息失败！");
        }
        return "发送成功！";
    }

    @RequestMapping(value = "/send/{topic}", method = RequestMethod.GET)
    @ApiOperation(value = "发送消息", notes = "向Rabbit制定topic发送一条消息")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "m", value = "消息内容", required = true, dataType = "String")
    })
    public String testRabbit(@PathVariable String topic, @RequestParam("m") String message) {
        try {
            template.convertAndSend(topic, message);
            LOGGER.info("发送消息：{}到{}", message, topic);
        } catch (Exception e) {
            LOGGER.error("发送消息失败！");
        }
        return "发送成功！";
    }
}
