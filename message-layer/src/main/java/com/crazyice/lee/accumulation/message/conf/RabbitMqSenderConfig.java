package com.crazyice.lee.accumulation.message.conf;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * rabbitmq的队列配置bean，从application.yml获取
 **/
@Configuration
public class RabbitMqSenderConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(RabbitMqSenderConfig.class);

    @Value(value = "${exchangeName}")
    private String exchangeName;

    @Value(value = "${queueName}")
    private String queueName;

    @Value(value = "${queueTopic1}")
    private String queueTopic1;

    @Value(value = "${queueTopic2}")
    private String queueTopic2;

    @Bean
    public TopicExchange exchange() {
        return new TopicExchange(exchangeName);
    }

    @Bean
    public Queue queue() {
        return new Queue(queueName);
    }

    @Bean(name = "${queueTopic1}")
    public Queue queueMessage1() {
        return new Queue(queueTopic1);
    }

    @Bean(name = "${queueTopic2}")
    public Queue queueMessage2() {
        return new Queue(queueTopic2);
    }

    @Bean
    Binding bindingExchangeMessage(@Qualifier("${queueTopic1}") Queue queueMessage1, TopicExchange exchange) {
        return BindingBuilder.bind(queueMessage1).to(exchange).with("topic.message");
    }

    @Bean
    Binding bindingExchangeMessages(@Qualifier("${queueTopic2}") Queue queueMessage2, TopicExchange exchange) {
        return BindingBuilder.bind(queueMessage2).to(exchange).with("topic.#");//*表示一个词,#表示零个或多个词
    }
}
