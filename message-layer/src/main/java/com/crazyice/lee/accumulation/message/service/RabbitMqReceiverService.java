package com.crazyice.lee.accumulation.message.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
public class RabbitMqReceiverService {
    private static final Logger LOGGER = LoggerFactory.getLogger(RabbitMqReceiverService.class);

    @RabbitListener(queues = "${queueName}")
    public void processC(String s) {
        LOGGER.info("接收到的消息：" + s);
    }

    @RabbitListener(queues = "${queueTopic1}")    //监听器监听指定的Queue
    public void process1(String str) {
        LOGGER.info("message1:" + str);
    }

    @RabbitListener(queues = "${queueTopic2}")    //监听器监听指定的Queue
    public void process2(String str) {
        LOGGER.info("message2:" + str);
    }
}
